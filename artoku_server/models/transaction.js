const mongoose = require('mongoose')
const { Schema } = mongoose

const transaction = new Schema({
    id: String,
    useremail: String,
    payee: String,
    amount: Number,
    type: String,
    category: String,
    description: String,
    createdAt: Date,
})

const Transaction = mongoose.model('transactions', transaction)
module.exports = { Transaction }