const mongoose = require('mongoose')
const { Schema } = mongoose

const user = new Schema({
    id: Number,
    firstname: String,
    lastname: String,
    useremail: String,
    username: String,
    address: String,
    password: String,
    createdAt: Date,
    balance: Number,
    updatedAt: Date,
    phone: String,
    failed_attempt: Number, 
    locked_until: Date,
    payee: Array,
    pinUser: String
})

const User = mongoose.model('users', user)
module.exports = { User }