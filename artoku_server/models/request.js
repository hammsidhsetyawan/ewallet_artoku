const mongoose = require('mongoose')
const { Schema } = mongoose

const request = new Schema({
    id: String,
    useremail: String,
    emailpayee: String,
    payee: String,
    amount: Number,
    type: String,
    description: String,
    createdAt: Date,
    status: String
})

const Request = mongoose.model('request', request)
module.exports = { Request }