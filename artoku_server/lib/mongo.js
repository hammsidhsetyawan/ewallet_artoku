const mongoose = require('mongoose')
const database = 'mongodb://localhost:27017/artoku_dev'

const mongodb = async () => {
    try {
        await mongoose.connect(database, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        })
        mongoose.set('debug', true)
        console.log(database)        
    } catch (err) {
        console.error(err.message)
        process.exit(1)
    }
}

module.exports = { mongodb }