const { User } = require('../models/user')
const { Transaction } = require('../models/transaction')
const md5 = require('md5')
const { checkEmail, checkUsername } = require('../lib/checker')

const viewHistory = async (email,limit) => {
    return await Transaction.find({useremail: email}).sort('-createdAt').limit(limit).lean()
}

const addTransaction = async (email, payload, type) => {
        const trxId = (type) => {
            return type ==='debit' ? 'DB' :
            type === 'credit' ? 'CR' :
            type === 'topup' ? 'TU' : null
        }
        const payee = (source) => {
            return source === 'credit' ? 'Credit Card' :
            source === 'bank' ? 'Bank' :
            payload.useremail === email ?
            payload.payee : payload.useremail
        }

        const newTransaction = new Transaction({
            id: trxId(type)+"-"+Date.now(),
            useremail: email,
            payee: payee(payload.source),
            amount: parseInt(payload.amount),
            category: payload.category ? payload.category: '',
            type: type,
            description: payload.description ? payload.description: '',
            createdAt: Date.now(),
        }) 
        newTransaction.save()
        return newTransaction
}

exports.getBalance = async function (request, h) {
    try {
        const result = await User.findOne({useremail: request.params.useremail})
        return h.response(result).code(200)
    }
    catch (err) {
        return h.response(
            [
                {
                    auth: false,
                    message: err
                }
            ]
        )
    }
}

exports.updateBalance = async function (request, h) {
    try {
        const userData = await User.findOne({useremail: request.params.useremail})
        const result = await User.findOneAndUpdate({ useremail: request.params.useremail }, { balance: userData.balance + parseInt(request.payload.amount)}).lean()
        const transaction = await addTransaction(request.params.useremail, request.payload, "topup")
        return h.response(transaction).code(200)
    }
    catch (err) {
        return h.response(
            [
                {
                    auth: false,
                    message: err
                }
            ]
        )
    }

}

exports.getHistory = async (request, h) => {
    params = request.params
    if((await checkEmail(params.useremail))!== null){
        try{
            const history = await viewHistory(params.useremail, parseInt(params.limit))
            return h.response(history).code(200)
        } catch (err) {
            return h.response({message: "Fetch history failed.", error: err}).code(500)
        }
    }else{
        return h.response({message: "Email not found"}).code(404)
    }
}

exports.addPayment = async (request,h) => {
    try{
        const {payload} = request
        const userData = await User.findOne({ useremail: payload.useremail }).lean();
        if(payload.pinUser === userData.pinUser){
            await User.findOneAndUpdate({useremail: payload.payee}, {$inc: {balance: parseInt(payload.amount)}}).lean()
            await User.findOneAndUpdate({useremail: payload.useremail}, {$inc: {balance: -1 * parseInt(payload.amount)}}).lean()
            await addTransaction(payload.payee, payload, "credit")
            await addTransaction(payload.useremail, payload, "debit")
            return h.response({ message: "Payment success"})
        } else {
            return h.response({ message: "Pin not valid" })
        }

    } catch {

    }
}

exports.getFilterHistory = async (request, h) => {
    return await Transaction.aggregate([
        { $addFields: { "year": { $year: "$createdAt" } } },
        { $match: { year: parseInt(request.params.year), useremail: request.params.useremail } }
    ]);
}
exports.getAllHistory = async (request, h) => {
    try {
        const result = await Transaction.find({ useremail: request.params.useremail }).sort('-createdAt')
        return h.response(result).code(200)
    }
    catch (err) {
        return h.response(
            [
                {
                    auth: false,
                    message: err
                }
            ]
        )
    }
}