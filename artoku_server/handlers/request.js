const { User } = require('../models/user')
const { Request } = require('../models/request')
const { checkEmail, checkUsername } = require('../lib/checker')

newRequest = async (payload) => {
    const newRequest = new Request({
        id: Date.now(),
        useremail: payload.useremail,
        emailpayee: payload.emailpayee,
        amount: parseInt(payload.amount),
        description: payload.description,
        status: "pending",
        createdAt: Date.now()
    })
    newRequest.save()
    return newRequest
}

exports.sendRequest = async (request, h) => {
    const {payload} = request
    await newRequest(payload)
    return h.response({ message: "Request success"}).code(200)
}

exports.getSentRequests = async (request, h) => {
    try {
        const result = await Request.find({useremail: request.params.useremail}).sort('-createdAt').lean()
        return h.response(result).code(200)
    }
    catch (err) {
        return h.response(
            [
                {
                    auth: false,
                    message: err
                }
            ]
        )
    }
}

exports.getReceivedRequests = async (request, h) => {
    try {
        const result = await Request.find({emailpayee: request.params.useremail}).sort('-createdAt').lean()
        return h.response(result).code(200)
    }
    catch (err) {
        return h.response(
            [
                {
                    auth: false,
                    message: err
                }
            ]
        )
    }
}

exports.updateRequestStatus = async (request, h) => {
    try {
        console.log(request.params,);
        const result = await Request.findOneAndUpdate({id: request.params.id},{status: "completed"}).lean()
        
        return h.response(result).code(200)
    }
    catch (err) {
        return h.response(
            [
                {
                    auth: false,
                    message: err
                }
            ]
        )
    }
}