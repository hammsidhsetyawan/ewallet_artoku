const { User } = require('../models/user')
const { checkEmail, checkUsername } = require('../lib/checker')

exports.findUser = async (request, h) => {
    const { contain } = request.query
    return await User.find({}, { username: 1, useremail: 1 }).sort({ username: 1 })
}

exports.addpayee = async (request, h) => {
    const { useremail } = request.params
    const { payload } = request
    return User.findOneAndUpdate({ 'useremail': useremail }, { $push: { payee: { 'useremail': payload.useremail, 'username': payload.username } } }, { new: true })
        .then(res => {
            if (!res) {
                return h.response({ message: "Email not found" })
            }
            return h.response(res).code(202)
        })
}

exports.getPayee = async (request, h) => {
    const { useremail } = request.params
    const user = await User.findOne({ useremail: useremail }, { 'payee': 1 })
    return user.payee
}

exports.findUserPayee = async (request, h) => {
    const { useremail } = request.params
    const user = await User.findOne({ useremail: useremail }, { 'payee': 1 }).lean()
    console.log(user.payee)
    let list = [];
    let input = user.payee

    list = input.reduce((accumulator, currentValue, index, array) => {
        accumulator.push(  input[index].useremail );
        return accumulator;
    }, [])

    console.log(list)

    return await User.find({ useremail: { "$nin": list } }, { username: 1, useremail: 1 }).sort({ username: 1 })

    
}
