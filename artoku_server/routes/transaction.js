const {getBalance ,getHistory, updateBalance,addPayment, getAllHistory, getFilterHistory } = require('../handlers/transaction')

module.exports = [
    {
      method: 'GET',
      path: '/transaction/{useremail}/history/{limit}',
      handler: getHistory
    },
    {
      method: 'GET',
      path: '/balances/{useremail}',
      handler: getBalance
    },
    {
      method: 'PUT',
      path: '/balances/{useremail}',
      handler: updateBalance
    },
    {
      method: 'GET',
      path: '/transaction/{useremail}/{year}',
      handler: getFilterHistory
},
{
      method: 'GET',
      path: '/transaction/{useremail}',
      handler: getAllHistory
},
    {
      method: 'POST',
      path: '/payment',
      handler: addPayment
    }
  ]