const { sendRequest, getSentRequests, getReceivedRequests, updateRequestStatus } = require('../handlers/request')

module.exports = [
    {
      method: 'POST',
      path: '/request',
      handler: sendRequest
    },
    {
      method: 'GET',
      path: '/request/{useremail}/sent',
      handler: getSentRequests
    },
    {
      method: 'GET',
      path: '/request/{useremail}/received',
      handler: getReceivedRequests
    },
    {
      method: 'PUT',
      path: '/request/{id}',
      handler: updateRequestStatus
    }
  ]