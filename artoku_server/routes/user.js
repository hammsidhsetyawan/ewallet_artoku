const { findUser, addpayee,getPayee,findUserPayee } = require('../handlers/user')

module.exports = [
      {
            method: 'GET',
            path: '/find',
            handler: findUser
      },
      {
            method: 'GET',
            path: '/find/{useremail}',
            handler: findUserPayee
      },
      {
            method: 'PUT',
            path: '/addpayee/{useremail}',
            handler: addpayee
      },
      {
            method: 'GET',
            path: '/payee/{useremail}',
            handler: getPayee
      }
]