const { signup, login, setpin } = require('../handlers/auth')
// const { get } = require('../handlers/get')
// const { updateBalances } = require('../handlers/updateBalances')
// const { getBalances } = require('../handlers/getBalances')
const {getPayee} = require('../handlers/user')

module.exports = [
  {
    method: 'POST',
    path: '/signup',
    handler: signup
  },
  {
    method: 'POST',
    path: '/login',
    handler: login
  },
  {
    method: 'PUT',
    path: '/setpinuser/{useremail}',
    handler: setpin
  }
]
