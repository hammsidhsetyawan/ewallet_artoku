const auth = require('./auth')
const user = require('./user')
const transaction = require('./transaction')
const request = require('./request')

module.exports = [].concat(auth,transaction,user,request)
