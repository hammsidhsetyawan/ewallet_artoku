const config = require('./config/config.json')
const routes = require('./routes')
const Hapi = require('@hapi/hapi')
const {mongodb} = require('./lib/mongo')
const Qs = require('qs')
const Path = require('path')
// require('dotenv').config()
// const API_address=config[process.env.NODE_ENV.toLowerCase()].host;

const start = async () => {

    mongodb()

    const server = new Hapi.Server({
      port: 8000,
      host: '0.0.0.0',
      query: {parser: query => Qs.parse(query)},
      routes: {
        files: {
          relativeTo: Path.join(__dirname, './contents/')
        }
      }
    });
  
    await server.register({
      plugin: require('hapi-pino'),
      options: {
        prettyPrint: process.env.NODE_ENV !== 'production',
        stream: './logs.log',
        redact: ['req.headers.authorization'],
      },
    });
    
    server.route(routes)
  
    await server.start();
  
    process.on('unhandledRejection', err => {
      console.log(err);
      process.exit(1);
    });
    // console.log(`ArtoKu server running at: ${server.port}`);
    return server;
  };
  
  module.exports = {start};
  