const Lab = require('@hapi/lab');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { expect } = require('@hapi/code')
const { start } = require('../server');

describe('GET /', () => {
      let server;
      const useremail = `mail${Date.now()}@mail.com`
      const username =  `name${Date.now()}`

      beforeEach(async () => {
            server = await start();
      });

      afterEach(async () => {
            await server.stop();
      });

      it('responds "/login" call (no payload) with HTTP 400 ', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
            })
            expect(res.statusCode).to.equal(400)
      })

      it('responds "/login" call (unregistered email) with HTTP 409', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
                  payload: {
                        useremail: 'test@mail.co'
                  }
            })
            expect(res.statusCode).to.equal(404)
      })

      it('responds "/login" call (unregistered email) with correct message', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
                  payload: {
                        useremail: 'test@mail.co'
                  }
            })
            expect(res.result.message).to.equal('Email not registered')
      })

      it('responds "/login" call (registered email and correct password) with correct message', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
                  payload: {
                        useremail: 'test@mail.com',
                        password: "cc03e747a6afbbcbf8be7668acfebee5",
                  }
            })
            expect(res.result.message).to.equal('Login success')
      })

      it('responds "/login" call (with registered email and correct password) with HTTP 200', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
                  payload: {
                        useremail: 'test@mail.com',
                        password: "cc03e747a6afbbcbf8be7668acfebee5",
                  }
            })
            expect(res.statusCode).to.equal(200)
      })
      it('responds "/login" call (registered email and wrong password) with correct message', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
                  payload: {
                        useremail: 'test2@mail.com',
                        password: "cc03e747a6afbbcbf8be7668acfebee",
                  }
            })
            expect(res.result.message).to.contain('Invalid email')
      })

      it('responds "/login" call (with registered email and wrong password) with HTTP 401', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
                  payload: {
                        useremail: 'test@mail.com',
                        password: "cc03e747a6afbbcbf8be7668acfebee",
                  }
            })
            expect(res.statusCode).to.equal(401)
      })

      it('responds "/login" call (with locked email) with correct message', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/login',
                  payload: {
                        useremail: 'test_error@mail.com',
                        password: "cc03e747a6afbbcbf8be7668acfebee",
                  }
            })
            expect(res.result.message).to.contain("Your account is locked for 15 minutes")
      })

      it('responds "/signup" with valid email and username', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/signup',
                  payload: {
                        username: username,
                        useremail: useremail,
                        password: 'arto12345'
                  }
            });
            expect(res.statusCode).to.equal(200)
      })

      it('responds "/signup" with valid email and not available username', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/signup',
                  payload: {
                        username: 'awang',
                        useremail: useremail,
                        password: 'arto12345'
                  }
            });
            expect(res.statusCode).to.equal(403)
      })

      it('responds "/signup" with not available email and available username', async () => {
            const res = await server.inject({
                  method: 'POST',
                  url: '/signup',
                  payload: {
                        username: username,
                        useremail: 'awang@gmail.com',
                        password: 'arto12345'
                  }
            });
            expect(res.statusCode).to.equal(402)
      })







})
