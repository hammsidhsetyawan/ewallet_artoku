# ArtoKu - E-Wallet Payment Platform

ArtoKu is an e-wallet application which make it easy for you to transact via a mobile app.
The name ArtoKu itself is taken from an abbreviation, Arto in Javanese language means “ Money “, and Ku means “ Me “ . So ArtoKu means “ My Money ".

## Getting Started
Change the uri of api service for app located on artoku_app/lib/src/config/const.dart into your machine IP.
*    git clone [https://gitlab.com/hammsidhsetyawan/ewallet_artoku]
*    cd ewallet_artoku
*    cd ./artoku_server  && npm install
*    cd ./artoku_app && flutter pub get

## Run App 
***
**Run in folder artoku_server with command :**
```sh
npm start
```
**Run in folder artoku_app with command :**
```sh
flutter run
```

##  Features :
****
| App Features |
| ------ |
| User Registration |
| Login & Logout  |
| User Make a Security Pin |
| Transaction Summary in Dashboard |
| Top Up Money |
| Add Payee |
| Transaction History (Filter transactions by year) |
| Request and Receive Payment |
 
## Tools for make a this Application
 - Visual Studio Code
 - Android sdk 28
 - Node JS 10.0.0
 - Flutter 
 - MongoDB

 ## Language Programming Used
 - Flutter - Dart language
 - Node js - Hapi
 
## Development Set Up
Before you begin, you should  already downloaded the Android Studio SDK, Flutter and set it up correctly. You can find a guide on how to do this here: https://codelabs.developers.google.com/codelabs/flutter/#0.

## Author
 - Brian Esa Chrisnando 
 - Desi Suci Ramadhani 
 - Muh. Sidhiq Setyawan
 - Yosua Simatupang 