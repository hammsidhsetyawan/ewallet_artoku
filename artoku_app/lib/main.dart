// import 'package:artoku_app/screens/dashboard/home_screen.dart';
import 'package:artoku_app/screens/session/login_screen.dart';
import 'package:flutter/material.dart';

void main({String env}) async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        home: LoginScreen());
  }
}
