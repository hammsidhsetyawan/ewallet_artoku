// import 'package:artoku_app/screens/dashboard/blank_screen.dart';
import 'package:artoku_app/screens/dashboard/payee_screen.dart';
import 'package:artoku_app/screens/dashboard/initial_payment/payment_screen.dart';
import 'package:artoku_app/screens/dashboard/request_payment/req_payment_section.dart';
import 'package:artoku_app/screens/dashboard/transaction_detail_screen.dart';
import 'package:artoku_app/screens/dashboard/transaction_history.dart';
import 'package:artoku_app/src/blocs/payRequestBloc.dart';
import 'package:artoku_app/src/blocs/transactionBloc.dart';
import 'package:artoku_app/src/models/payRequestModel.dart';
import 'package:artoku_app/src/models/transactionModel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:artoku_app/screens/session/login_screen.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/screens/dashboard/topUp/topUpPage.dart';
import 'package:artoku_app/screens/dashboard/request_payment/req_payment_page.dart';
import 'package:strings/strings.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  PayRequestBloc payRequestBloc = PayRequestBloc();
  Future<Null> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('useremail', null);
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));
  }

  Future delay() async {
    await new Future.delayed(const Duration(seconds: 10));
  }

  @override
  Widget build(BuildContext context) {
    bloc.getBalanceUser();
    blocs.updateFundMessage("");
    blocs.updateToken(0);
    blocs.updateOtpMessage(null);
    TransactionBloc transactionBloc = TransactionBloc();
    delay();
    return Scaffold(
      body: Container(
        color: Colors.blueAccent,
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            //Container for top data
            Container(
              margin: EdgeInsets.symmetric(horizontal: 32, vertical: 32),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      StreamBuilder(
                        stream: bloc.getStreamBalanceUser,
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                              NumberFormat.simpleCurrency(
                                      locale: 'id', decimalDigits: 0)
                                  .format(snapshot.data),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 24,
                                  fontWeight: FontWeight.w700),
                            );
                          } else if (snapshot.hasError) {
                            return Text(snapshot.error.toString());
                          }
                          return Center(child: CircularProgressIndicator());
                        },
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            // StreamBuilder<List<PayRequest>>(
                            //   stream: payRequestBloc.payRequestListNow,
                            //   builder: (BuildContext context,
                            //       AsyncSnapshot<List<PayRequest>> snapshot) {
                            //     if (snapshot.hasData) {
                            //       if (snapshot.data.length == 0) {
                            //         return IconButton(
                            //           icon: Icon(
                            //             Icons.notifications_active,
                            //             color: Colors.lightBlue[900],
                            //             size: 30,
                            //           ),
                            //           onPressed: () {},
                            //         );
                            //       } else
                            //         return IconButton(
                            //           icon: Icon(
                            //             Icons.notifications,
                            //             color: Colors.lightBlue[900],
                            //             size: 30,
                            //           ),
                            //           onPressed: () {},
                            //         );
                            //     } else if (snapshot.hasError) {
                            //       return Text(snapshot.error.toString());
                            //     }
                            //     return Center(
                            //         child: CircularProgressIndicator());
                            //   },
                            // ),
                            IconButton(
                              icon: Icon(
                                Icons.notifications,
                                color: Colors.lightBlue[900],
                                size: 30,
                              ),
                              onPressed: () {
                                // Navigator.push(
                                //   context,
                                //   MaterialPageRoute(
                                //     builder: (context) =>
                                //         BlankScreen(),
                                //     // Pass the arguments as part of the RouteSettings. The
                                //     // DetailScreen reads the arguments from these settings.

                                //     ),
                                //   );
                              },
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            InkWell(
                              onTap: () {
                                logout();
                              },
                              child: CircleAvatar(
                                radius: 25,
                                backgroundColor: Colors.white,
                                child: ClipOval(
                                  child: Image.asset(
                                    "assets/dp.jpg",
                                    fit: BoxFit.contain,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  Text(
                    "Available Balance",
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 16,
                        color: Colors.blue[100]),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PaymentScreen()),
                                );
                                // show
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(243, 245, 248, 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(18))),
                                child: Icon(
                                  Icons.arrow_upward,
                                  color: Colors.blue[900],
                                  size: 30,
                                ),
                                padding: EdgeInsets.all(12),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Send",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                  color: Colors.blue[100]),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ReqPaymentSection()),
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(243, 245, 248, 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(18))),
                                child: Icon(
                                  Icons.chat,
                                  color: Colors.blue[900],
                                  size: 30,
                                ),
                                padding: EdgeInsets.all(12),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Request",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                  color: Colors.blue[100]),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PayeeScreen()),
                                );
                                // showSearch(context: context, delegate: DataSearch());
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(243, 245, 248, 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(18))),
                                child: Icon(
                                  Icons.perm_contact_calendar,
                                  color: Colors.blue[900],
                                  size: 30,
                                ),
                                padding: EdgeInsets.all(12),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Payee List",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                  color: Colors.blue[100]),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                blocs.updateType("topup");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => TopUpPage()),
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(243, 245, 248, 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(18))),
                                child: Icon(
                                  Icons.attach_money,
                                  color: Colors.blue[900],
                                  size: 30,
                                ),
                                padding: EdgeInsets.all(12),
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Topup",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                  color: Colors.blue[100]),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),

            //draggable sheet
            DraggableScrollableSheet(
              builder: (context, scrollController) {
                return Container(
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(243, 245, 248, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40))),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 24,
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Recent Transactions",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 24,
                                    color: Colors.black),
                              ),
                              GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              TransactionHistory()),
                                    );
                                  },
                                  child: Text(
                                    "See all",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15,
                                        color: Colors.green[700]),
                                  )),
                            ],
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 32),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        StreamBuilder<List<Transaction>>(
                          stream: transactionBloc.transactionListNow,
                          builder: (BuildContext context,
                              AsyncSnapshot<List<Transaction>> snapshot) {
                            if (snapshot.hasData) {
                              if (snapshot.data.length == 0) {
                                return Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: 200,
                                    margin: EdgeInsets.only(top: 150),
                                    child: Text(
                                      "Sadly, there's nothing here yet.",
                                      // textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 15,
                                          color: Colors.black),
                                    ),
                                  ),
                                );
                              } else
                                return buildTransactionHistoryList(snapshot);
                            } else if (snapshot.hasError) {
                              return Text(snapshot.error.toString());
                            }
                            return Center(child: CircularProgressIndicator());
                          },
                        ),
                      ],
                    ),
                    controller: scrollController,
                  ),
                );
              },
              initialChildSize: 0.65,
              minChildSize: 0.65,
              maxChildSize: 1,
            )
          ],
        ),
      ),
    );
  }

  Widget buildTransactionHistoryList(AsyncSnapshot snapshot) {
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context, int index) {
        Transaction _transaction = snapshot.data[index];
        var transactionDate = _transaction.createdAt;
        var formatter = new DateFormat('d MMM y');
        String formatted = formatter.format(transactionDate);

        return InkWell(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Row(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.all(Radius.circular(18))),
                  child: Icon(
                    _transaction.type == 'debit'
                        ? Icons.arrow_upward
                        : Icons.arrow_downward,
                    color: _transaction.type != 'debit'
                        ? Colors.lightGreen
                        : Colors.red[400],
                  ),
                  padding: EdgeInsets.all(12),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        _transaction.type == 'debit'
                            ? "Outgoing Transfer"
                            : _transaction.type == 'credit'
                                ? "Incoming Transfer"
                                : "Top Up",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[900]),
                      ),
                      Text(
                        _transaction.type == 'topup'
                            ? "${capitalize(_transaction.payee)}"
                            : "${_transaction.payee}",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[500]),
                      ),
                      Text(
                        _transaction.type == 'topup'
                            ? "Top Up from " + _transaction.payee
                            : "${_transaction.description}",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[500]),
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      _transaction.type != 'debit'
                          ? "+" +
                              NumberFormat.simpleCurrency(
                                      locale: 'id', decimalDigits: 0)
                                  .format(_transaction.amount)
                          : "-" +
                              NumberFormat.simpleCurrency(
                                      locale: 'id', decimalDigits: 0)
                                  .format(_transaction.amount),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: _transaction.type != 'debit'
                            ? Colors.lightGreen
                            : Colors.red[400],
                      ),
                    ),
                    Text(
                      "$formatted",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                          color: Colors.grey[500]),
                    ),
                  ],
                ),
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TransactionDetailScreen(),
                // Pass the arguments as part of the RouteSettings. The
                // DetailScreen reads the arguments from these settings.
                settings: RouteSettings(
                  arguments: _transaction,
                ),
              ),
            );
          },
        );
      },
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      controller: ScrollController(keepScrollOffset: false),
    );
  }
}
