import 'package:artoku_app/screens/dashboard/home_screen.dart';
import 'package:artoku_app/screens/dashboard/topUp/request_detail_screen.dart';
import 'package:artoku_app/screens/dashboard/transaction_detail_screen.dart';
import 'package:artoku_app/src/blocs/payRequestBloc.dart';
import 'package:artoku_app/src/models/payRequestModel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:strings/strings.dart';

class RequestListScreen extends StatefulWidget {
  @override
  _RequestListScreenState createState() => _RequestListScreenState();
}

class _RequestListScreenState extends State<RequestListScreen> {
  PayRequestBloc payRequestBloc = PayRequestBloc();
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: DraggableScrollableSheet(
          builder: (context, scrollController) {
            return Container(
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Payment Requests",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 24,
                                    color: Colors.black),
                              ),
                              Text(
                                "Received and sent payment requests",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 14,
                                    color: Colors.grey),
                              ),
                            ],
                          ),
                        ],
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 32),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      child: Text(
                        "Received Requests",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                            color: Colors.black),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 32),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    StreamBuilder<List<PayRequest>>(
                      stream: payRequestBloc.receivedPayRequestListNow,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<PayRequest>> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data.length == 0) {
                            return Align(
                              alignment: Alignment.center,
                              child: Container(
                                height: 200,
                                margin: EdgeInsets.only(top: 100),
                                child: Text(
                                  "It's so lonely, there's nothing here.",
                                  // textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15,
                                      color: Colors.black),
                                ),
                              ),
                            );
                          } else
                            return buildPayRequestList(snapshot, "received");
                        } else if (snapshot.hasError) {
                          return Text(snapshot.error.toString());
                        }
                        return Center(child: CircularProgressIndicator());
                      },
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Container(
                      child: Text(
                        "Sent Requests",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 20,
                            color: Colors.black),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 32),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    StreamBuilder<List<PayRequest>>(
                      stream: payRequestBloc.sentPayRequestListNow,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<PayRequest>> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data.length == 0) {
                            return Align(
                              alignment: Alignment.center,
                              child: Container(
                                height: 200,
                                margin: EdgeInsets.only(top: 150),
                                child: Text(
                                  "It's so lonely, there's nothing here.",
                                  // textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15,
                                      color: Colors.black),
                                ),
                              ),
                            );
                          } else
                            return buildPayRequestList(snapshot, "sent");
                        } else if (snapshot.hasError) {
                          return Text(snapshot.error.toString());
                        }
                        return Center(child: CircularProgressIndicator());
                      },
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(243, 245, 248, 1),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(40),
                      topLeft: Radius.circular(40))),
            );
          },
          initialChildSize: 0.95,
          minChildSize: 0.95,
        ),
      ),
    );
  }

// TODO: navigate to detail and add button
  Widget buildPayRequestList(AsyncSnapshot snapshot, String type) {
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context, int index) {
        PayRequest _payRequest = snapshot.data[index];
        var requestDate = _payRequest.createdAt;
        var formatter = new DateFormat('d MMM y');
        String formatted = formatter.format(requestDate);

        return InkWell(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Row(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.all(Radius.circular(18))),
                  child: Icon(
                    type == 'sent' ? Icons.trending_up : Icons.trending_down,
                    color:
                        type == 'sent' ? Colors.lightGreen : Colors.yellow[700],
                  ),
                  padding: EdgeInsets.all(12),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        type == "received" ? "Request from" : "Request to",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[900]),
                      ),
                      Text(
                        type == "received"
                            ? _payRequest.useremail
                            : _payRequest.emailpayee,
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[500]),
                      ),
                      Text(
                        type == "sent"
                            ? ""
                            : _payRequest.status == 'pending'
                                ? "Tap to respond"
                                : "",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[500]),
                      )
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      NumberFormat.simpleCurrency(
                              locale: 'id', decimalDigits: 0)
                          .format(_payRequest.amount),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: Colors.grey[700],
                      ),
                    ),
                    Text(
                      "$formatted",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                          color: Colors.grey[500]),
                    ),
                    Text(
                      _payRequest.status,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: _payRequest.status == 'completed'
                            ? Colors.lightGreen
                            : Colors.yellow[800],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => RequestDetailScreen(),
                // Pass the arguments as part of the RouteSettings. The
                // DetailScreen reads the arguments from these settings.
                settings: RouteSettings(
                  arguments: _payRequest,
                ),
              ),
            );
          },
        );
      },
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      controller: ScrollController(keepScrollOffset: false),
    );
  }
}
