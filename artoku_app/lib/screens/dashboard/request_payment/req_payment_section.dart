import 'package:artoku_app/screens/dashboard/payee_screen.dart';
import 'package:artoku_app/screens/dashboard/request_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:artoku_app/src/config/const.dart';

class ReqPaymentSection extends StatelessWidget {
  final appTitle = 'Bank Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      initialRoute: '/',
      home: MyHomePage(),
      routes: {
        '/addPayeePage': (context) => PayeeScreen(),
        '/payRequestList': (context) => RequestListScreen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State createState() => MyHomePageState();
}

class User {
  const User(this.name);

  final String name;
}

class MyHomePageState extends State<MyHomePage> {
  int _amount;
  String _description;

  final title = "Request Payment";
  String dropdownValue = 'Ahmad';
  String amount = "";
  bool ontapped = false;

  String _mySelection;

  List data = List(); //edited line

  Future<String> getSWData() async {
    String email = blocs.navigationProvider.currentEmail;
    final String url = "$base_url/payee/$email";
    var res = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      data = resBody;
    });

    print(resBody);

    return "Sucess";
  }

  @override
  void initState() {
    super.initState();
    this.getSWData();
  }

  @override
  Widget build(BuildContext context) {
    Widget teks() {
      Widget teks1 =
          Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Container(
            child: Text(
          "Please select a payee",
          style: TextStyle(color: Colors.red),
        ))
      ]);

      if (_mySelection == null) {
        return teks1;
      } else {
        return Text("");
      }
    }

    Widget teks2() {
      Widget teks2 =
          Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Container(
            child: Text(
          "Amount must be between 1.000 - 100.000.000",
          style: TextStyle(color: Colors.red),
        ))
      ]);
      try {
        if ((int.tryParse(amount) < 1000) ||
            (int.tryParse(amount) > 100000000) ||
            (int.tryParse(amount) == null)) {
          return teks2;
        } else {
          return Text("");
        }
      } catch (err) {
        return teks2;
      }
    }

    Widget teks3() {
      Widget teks3 =
          Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        Container(
            child: Text(
          "Please fill the description, maximum 200 character",
          style: TextStyle(color: Colors.red),
        ))
      ]);

      if ((_description == null) ||
          (_description.length < 1) ||
          (_description.length > 200)) {
        return teks3;
      } else {
        return Text("");
      }
    }

    return Scaffold(
        appBar: AppBar(title: Text(title)),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: DropdownButton<String>(
                          hint: Text('Please select a payee'),
                          value: _mySelection,
                          onChanged: (newValue) {
                            setState(() {
                              _mySelection = newValue;
                            });

                            blocs.updateEmailReq(newValue);
                            print(blocs.navigationProvider.currentEmailReq);
                          },
                          items: data.map((value) {
                            return DropdownMenuItem(
                              value: value['useremail'].toString() ?? '',
                              child: Text(value['useremail'] ?? ''),
                            );
                          }).toList(),
                        ),
                      ),
                      Container(
                        child: RaisedButton(
                          child: Text("Add Payee"),
                          onPressed: () {
                            return Navigator.pushNamed(
                                context, '/addPayeePage');
                          },
                          splashColor: Colors.blueAccent,
                        ),
                      )
                    ],
                  ),
                  teks()
                ],
              ),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          child: Flexible(
                              child: TextFormField(
                                  decoration: InputDecoration(
                                      labelText: "Please enter amount"),
                                  keyboardType: TextInputType.number,
                                  onTap: () => setState(() {
                                        ontapped = true;
                                      }),
                                  onChanged: (text) => {
                                        setState(() {
                                          amount = text;
                                          ontapped = true;
                                        }),
                                        blocs.updateAmountReq(
                                            int.tryParse(text)),
                                        print(blocs.navigationProvider
                                            .currentAmountReq)
                                      }))),
                    ],
                  ),
                  teks2()
                ],
              ),
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          child: Flexible(
                              child: TextFormField(
                                  decoration:
                                      InputDecoration(labelText: "Description"),
                                  maxLines: 3,
                                  onChanged: (text) => {
                                        setState(() {
                                          _description = text;
                                        }),
                                        blocs.updateDescriptionReq(text),
                                        print(_description)
                                      }))),
                    ],
                  ),
                  teks3()
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      margin: const EdgeInsets.all(10.0),
                      child: RaisedButton(
                        child: Text("Request"),
                        onPressed: () => {
                          if ((int.tryParse(amount) < 1000) ||
                              (int.tryParse(amount) > 10000000) ||
                              (_mySelection == null) ||
                              (_description == null) ||
                              (_description.length < 1) ||
                              (_description.length > 200))
                            {null}
                          else
                            {
                              bloc.addRequest(),
                              Navigator.pushNamed(context, '/payRequestList')
                            }
                        },
                        splashColor: Colors.blueAccent,
                      )),
                ],
              )
            ],
          ),
        ));
  }
}
