import 'package:artoku_app/screens/dashboard/home_screen.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:artoku_app/src/blocs/transactionBloc.dart';
// import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/config/const.dart';
import 'package:artoku_app/src/models/payRequestModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:intl/intl.dart';

class RespondPaymentScreen extends StatefulWidget {
  @override
  _RespondPaymentScreenState createState() => _RespondPaymentScreenState();
}

class _RespondPaymentScreenState extends State<RespondPaymentScreen> {
  List data = List();
  String payee,
      amount,
      category,
      description,
      pin,
      _responseMessage,
      payRequestId;
  List<String> _categorys = [
    'Food',
    'Entertainment',
    'Shopping',
    'Groceries',
    'Miscellaneous'
  ];
  int balance = blocs.navigationProvider.currentBalance;

  @override
  void dispose() {
    transBloc.dispose();
    super.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<String> getPayee() async {
    String email = blocs.navigationProvider.currentEmail;
    final String url = "$base_url/payee/$email";
    var res = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      data = resBody;
    });

    print(resBody);

    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getPayee();
  }

  pinScreen() {
    Center(
        child: Row(
      children: <Widget>[
        TextFormField(
          obscureText: true,
          decoration: new InputDecoration(hintText: 'Your PIN'),
          maxLength: 4,
          onSaved: (input) => pin = input,
        ),
        MaterialButton(
          textColor: Colors.white,
          child: Text("Transfer"),
          onPressed: () {
            _submitPayment();
          },
          splashColor: Colors.blueAccent,
        )
      ],
    ));
  }

  void _submitPayment() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      final _updateStatus = await http.put("$base_url/request/$payRequestId");
      final _response =
          await transBloc.addPayment(payee, amount, category, description, pin);
      _responseMessage = _response["message"];
      print(_responseMessage);

      _responseMessage = _response["message"];
      print(_response["message"]);
      if (_response["message"] == 'Payment success') {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
        );
      } else if (_response["message"] != 'Payment success') {
        // setState(() => _triggerSnack = true);
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    PayRequest _payRequest = ModalRoute.of(context).settings.arguments;
    setState(() {
      payee = _payRequest.useremail;
      payRequestId = _payRequest.payRequestId;
    });
    return Scaffold(
      appBar: AppBar(title: Text('Send Payment'), backgroundColor: Colors.blue),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  DropdownButton<String>(
                    value: _payRequest.useremail,
                    hint: Text(_payRequest.useremail),
                    onChanged: null,
                    items: data.map((value) {
                      return DropdownMenuItem(
                        value: value['useremail'].toString(),
                        child: Text(value['useremail'] ?? ''),
                      );
                    }).toList(),
                  ),
                  Text(
                    'Available balance: ' +
                        NumberFormat.simpleCurrency(
                                locale: 'id', decimalDigits: 0)
                            .format(balance),
                    style: TextStyle(fontSize: 12.0),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        hintText: 'Amount will be transfer',
                        // labelText: amountValidator()
                        labelText: 'Amount'),
                    keyboardType: TextInputType.number,
                    validator: validateAmount,
                    onSaved: (input) => amount = input,
                    initialValue: _payRequest.amount.toString(),
                    enabled: false,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        hintText: 'Description here', labelText: 'Description'
                        // labelText: descValidator()
                        ),
                    keyboardType: TextInputType.multiline,
                    validator: validateDesc,
                    onSaved: (input) => description = input,
                  ),
                  DropdownButton(
                    hint: Text('Please choose a category'),
                    value: category,
                    onChanged: (newValue) {
                      setState(() {
                        category = newValue;
                      });
                    },
                    items: _categorys.map((location) {
                      return DropdownMenuItem(
                        child: new Text(location),
                        value: location,
                      );
                    }).toList(),
                  ),
                  TextFormField(
                    obscureText: true,
                    decoration: new InputDecoration(hintText: 'Your PIN'),
                    maxLength: 4,
                    onSaved: (input) => pin = input,
                  ),
                  RaisedButton(
                    child: Text('Submit'),
                    onPressed: () async {
                      await _submitPayment();
                    },
                  ),
                  // buttonHandler()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String validateAmount(String value) {
    var valueInt = int.tryParse(value);
    if (valueInt == null) {
      return "Please enter the amount ";
      // } else if (valueInt < 1000 || valueInt > 1000) {
      //   return "Amount must be 1.000 - 100.000.000";
    } else if (valueInt >= balance) {
      return "Amount must not higher than your balance";
    } else {
      return null;
    }
  }

  String validateDesc(String value) {
    if (value == null) {
      return "Please enter the description";
    } else if (value.length >= 200) {
      return "Description must be under 200 char";
    } else {
      return null;
    }
  }
}
