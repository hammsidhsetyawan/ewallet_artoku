import 'package:flutter/material.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:intl/intl.dart';
// import 'otpTopUpPage.dart';
// import 'topUpPage.dart';
import 'package:provider/provider.dart';
// import 'package:artoku_app/src/models/todoModels.dart';
import '../../../src/resources/userApiProvider.dart';
import 'dart:convert';
import 'package:artoku_app/screens/dashboard/home_screen.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/config/const.dart';
import 'package:artoku_app/src/models/UsersModels.dart';
import 'package:artoku_app/src/models/payRequestModel.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SentReqSection extends StatelessWidget {
  final appTitle = 'Bank Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      initialRoute: '/',
      home: MyHomePage(),
      routes: {
        // '/topUpPage': (context) => OtpTopUpPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  List<PayRequest> _list = [];
  var loading = false;

  Future<Null> fetchData() async {
    setState(() {
      loading = true;
    });
    _list.clear();
    String email = blocs.navigationProvider.currentEmail;
    final response = await http.get("$base_url/request/$email");
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        for (Map i in data) {
          _list.add(PayRequest.fromJson(i));
          loading = false;
        }
      });
    }
  }

  TextEditingController controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    fetchData();
    // bloc.fetchAllUsers();
    // bloc.addPayeeUser();
  }

  @override
  void dispose() {
    // bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sent Payment Request'),
        backgroundColor: Colors.blue,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : _list.length != 0
                    ? Expanded(
                        child: ListView.builder(
                          itemCount: _list.length,
                          itemBuilder: (context, i) {
                            final a = _list[i];
                            var date = a.createdAt;
                            var formatter =
                                new DateFormat('yyyy-MM-dd').format(date);
                            return Container(
                                padding: EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    ListTile(
                                        leading: Icon(
                                          a.status == 'pending'
                                              ? Icons.query_builder
                                              : Icons.check,
                                          color: a.status == 'pending'
                                              ? Colors.blue[300]
                                              : Colors.green,
                                        ),
                                        title: Text(
                                          a.amount.toString() ?? '',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                        ),
                                        subtitle: Text(a.emailpayee ?? ''),
                                        trailing: Column(
                                          children: <Widget>[
                                            Text(formatter ?? ''),
                                            Text(a.status)
                                          ],
                                        )),
                                  ],
                                ));
                          },
                        ),
                      )
                    : Text("Such empty."),
          ],
        ),
      ),
    );
  }
}
