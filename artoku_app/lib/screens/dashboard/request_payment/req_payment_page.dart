import 'package:artoku_app/screens/dashboard/request_payment/received_req_section.dart';
import 'package:flutter/material.dart';
import 'req_payment_section.dart';
import 'sent_req_section.dart';

class ReqPayment extends StatelessWidget {
  final appTitle = 'Top Up Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      home: MyHomePage(title: appTitle),
      routes: {
        '/reqPayment': (context) => ReqPaymentSection(),
        '/sentReq': (context) => SentReqSection(),
        '/receivedReq': (context) => ReceivedReqSection()
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;
  MyHomePage({Key key, this.title}) : super(key: key);
  @override
  State createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final String title = "Request Payment Page";
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Center(
        child: Stack(fit: StackFit.expand, children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.all(10.0),
                  child: Row(children: <Widget>[
                    RaisedButton(
                      child: Text("Request Payment"),
                      onPressed: () {
                        Navigator.pushNamed(context, '/reqPayment');
                      },
                      splashColor: Colors.blueAccent,
                    )
                  ])),
              Container(
                  margin: const EdgeInsets.all(10.0),
                  child: Row(children: <Widget>[
                    RaisedButton(
                      child: Text("Pending Request"),
                      onPressed: () {
                        Navigator.pushNamed(context, '/sentReq');
                      },
                      splashColor: Colors.blueAccent,
                    )
                  ])),
              Container(
                  margin: const EdgeInsets.all(10.0),
                  child: Row(children: <Widget>[
                    MaterialButton(
                      color: Colors.teal,
                      textColor: Colors.white,
                      child: Text("Received"),
                      onPressed: () {
                        Navigator.pushNamed(context, '/receivedReq');
                      },
                      splashColor: Colors.blueAccent,
                    )
                  ]))
            ],
          )
        ]),
      ),
    );
  }
}
