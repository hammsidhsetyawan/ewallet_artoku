import 'dart:convert';

import 'package:artoku_app/screens/dashboard/home_screen.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/config/const.dart';
import 'package:artoku_app/src/models/UsersModels.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SearchPayeeScreen extends StatefulWidget {
  @override
  _SearchPayeeScreenState createState() => _SearchPayeeScreenState();
}

class _SearchPayeeScreenState extends State<SearchPayeeScreen> {
  List<Users> _list = [];
  List<Users> _search = [];
  var loading = false;

  Future<Null> fetchData() async {
    setState(() {
      loading = true;
    });
    _list.clear();
    String email = blocs.navigationProvider.currentEmail;
    final response = await http.get("$base_url/find/$email");
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        for (Map i in data) {
          _list.add(Users.fromJson(i));
          loading = false;
        }
      });
    }
  }

  TextEditingController controller = new TextEditingController();

  onSearch(String text) async {
    _search.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _list.forEach((f) {
      if (f.useremail.contains(text) || f.username.contains(text)) {
        print(f.useremail);
        _search.add(f);
      } 
    });

    setState(() {});
  }
  

  @override
  void initState() {
    super.initState();
    fetchData();
    // bloc.fetchAllUsers();
    // bloc.addPayeeUser();
  }

  @override
  void dispose() {
    // bloc.dispose();
    super.dispose();
  }

  void _message(BuildContext context) {
    final message = SnackBar(
      content: Text('You have added Payee to your list'),
      backgroundColor: Colors.black,
      action: SnackBarAction(
        label: 'Ok',
        textColor: Colors.blue,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => HomeScreen()));

        // Navigator.pop(context);
        },
        
      ),
    );
    Scaffold.of(context).showSnackBar(message);
  }

  // void _notfound() {
  //   final _notfound = SnackBar(
  //     content: Text('not found'),
  //     backgroundColor: Colors.transparent,
  //     // action: SnackBarAction(
  //     //   label: 'Ok',
  //     //   textColor: Colors.purple,
  //     //   onPressed: () {
  //     //     Navigator.push(
  //     //         context, MaterialPageRoute(builder: (context) => HomeScreen()));
  //     //     // print('OK has been clicked');
  //     //   },
  //     // ),
  //   );
  //   Scaffold.of(context).showSnackBar(_notfound);
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search Payee'),
        backgroundColor: Colors.blue,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              color: Colors.blue,
              child: Card(
                child: ListTile(
                  leading: Icon(Icons.search),
                  title: TextField(
                    controller: controller,
                    onChanged: onSearch,
                    decoration: InputDecoration(
                        hintText: "Search", border: InputBorder.none),
                  ),
                  trailing: IconButton(
                    onPressed: () {
                      controller.clear();
                      onSearch('');
                    },
                    icon: Icon(Icons.cancel),
                  ),
                ),
              ),
            ),
            loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Expanded(
                    child: _search.length != 0 || controller.text.isNotEmpty
                        ? ListView.builder(
                            itemCount: _search.length,
                            itemBuilder: (context, i) {
                              final b = _search[i];
                              return Container(
                                  padding: EdgeInsets.all(10.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      ListTile(
                                        title: Text(
                                          b.username,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                        ),
                                        subtitle: Text(b.useremail),
                                        trailing: IconButton(
                                          onPressed: () {
                                            bloc.addPayeeUser(
                                                b.useremail, b.username);
                                            _message(context);
                                          },
                                          icon: Icon(Icons.add),
                                        ),
                                      ),
                                    ],
                                  ));
                            },
                          )
                        : ListView.builder(
                            itemCount: _list.length,
                            itemBuilder: (context, i) {
                              final a = _list[i];
                              return Container(
                                  padding: EdgeInsets.all(10.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      ListTile(
                                        title: Text(
                                          a.username,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                        ),
                                        subtitle: Text(a.useremail),
                                        trailing: IconButton(
                                          onPressed: () {
                                            bloc.addPayeeUser(
                                                a.useremail, a.username);
                                            _message(context);
                                          },
                                          icon: Icon(Icons.add),
                                        ),
                                      ),
                                    ],
                                  ));
                            },
                          ),
                  ),
          ],
        ),
      ),
    );
  }
}
