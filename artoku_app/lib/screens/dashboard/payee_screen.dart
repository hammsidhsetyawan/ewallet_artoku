import 'package:artoku_app/screens/dashboard/search_payee.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/models/UsersModels.dart';

import 'package:flutter/material.dart';

class PayeeScreen extends StatefulWidget {
  @override
  _PayeeScreenState createState() => _PayeeScreenState();
}

class _PayeeScreenState extends State<PayeeScreen> {
  Future delay() async {
    await new Future.delayed(const Duration(seconds: 10));
  }

  @override
  Widget build(BuildContext context) {
    UserBloc userBloc = UserBloc();
    delay();
    return Material(
      child: Container(
        color: Colors.blueAccent,
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: DraggableScrollableSheet(
          builder: (context, scrollController) {
            return Container(
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Payee List",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 24,
                                    color: Colors.black),
                              ),
                              Text(
                                "Your payee list",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 14,
                                    color: Colors.grey),
                              ),
                            ],
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.search,
                              color: Colors.lightBlue[900],
                              size: 30,
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SearchPayeeScreen()),
                              );
                            },
                          )
                        ],
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 32),
                    ),
                    SizedBox(
                      height: 16,
                    ),
// content here
                    StreamBuilder<List<Users>>(
                      stream: userBloc.payeeList,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<Users>> snapshot) {
                        // List<String> payees = snapshot.data;
                        if (snapshot.hasData) {
                          if (snapshot.data.length == 0) {
                            return Align(
                              alignment: Alignment.center,
                              child: Container(
                                height: 200,
                                margin: EdgeInsets.only(top: 150),
                                child: Text(
                                  "Please search, and find your friend",
                                  // textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 15,
                                      color: Colors.black),
                                ),
                              ),
                            );
                          } else
                            return buildPayeeList(snapshot);
                        } else if (snapshot.hasError) {
                          return Text(snapshot.error.toString());
                        }
                        return Center(child: CircularProgressIndicator());
                        // return Container(
                        //   child:
                        // );
                      },
                    ),
// content
                  ],
                ),
              ),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(243, 245, 248, 1),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(40),
                      topLeft: Radius.circular(40))),
            );
          },
          initialChildSize: 0.95,
          maxChildSize: 0.95,
          minChildSize: 0.95,
        ),
      ),
    );
  }

  Widget buildPayeeList(AsyncSnapshot snapshot) {
    return ListView.builder(
      itemCount: snapshot.data.length,
      itemBuilder: (BuildContext context, int index) {
        // final b = _search[i];
        Users _payee = snapshot.data[index];

        return InkWell(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: Row(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.all(Radius.circular(18))),
                  child: Icon(
                    Icons.person,
                    color: Colors.blueAccent,
                  ),
                  padding: EdgeInsets.all(12),
                ),
                SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        _payee.username,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[900]),
                      ),
                      Text(
                        _payee.useremail,
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            color: Colors.grey[500]),
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[],
                ),
              ],
            ),
          ),
          onTap: () {},
        );
      },
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      controller: ScrollController(keepScrollOffset: false),
    );
  }
}

