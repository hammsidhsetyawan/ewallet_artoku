import 'package:artoku_app/screens/dashboard/dashboard_screen.dart';
import 'package:artoku_app/screens/dashboard/home_screen.dart';
import 'package:artoku_app/screens/dashboard/search_payee.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:artoku_app/src/blocs/transactionBloc.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/config/const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:intl/intl.dart';

class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  List data = List();
  bool _isVisible = false;
  String payee, amount, category, description, pin, _responseMessage;
  List<String> _categorys = [
    'Food',
    'Entertainment',
    'Shopping',
    'Groceries',
    'Miscellaneous'
  ];
  int balance = blocs.navigationProvider.currentBalance;

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  @override
  void dispose() {
    transBloc.dispose();
    super.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<String> getPayee() async {
    String email = blocs.navigationProvider.currentEmail;
    final String url = "$base_url/payee/$email";
    var res = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      data = resBody;
    });

    // print(resBody);

    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getPayee();
  }

  void _submitPayment() async {
    if (_formKey.currentState.validate()) {
      showToast();
      _formKey.currentState.save();

      final _response =
          await transBloc.addPayment(payee, amount, category, description, pin);
      _responseMessage = _response["message"];
      print(bloc.payeeList);

      _responseMessage = _response["message"];
      // print(_response["message"]);
      if (_response["message"] == 'Payment success') {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
        );
      } else if (_response["message"] != 'Payment success') {
        // setState(() => _triggerSnack = true);
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            AppBar(title: Text('Send Payment'), backgroundColor: Colors.blue),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 500.0,
                        child: Row(
                          children: <Widget>[
                            DropdownButton<String>(
                              value: payee,
                              hint: Text('Please select a payee'),
                              onChanged: (newValue) {
                                setState(() {
                                  payee = newValue;
                                });
                                print(payee);
                              },
                              items: data.map((value) {
                                return DropdownMenuItem(
                                  value: value['useremail'].toString(),
                                  child: Text(value['useremail'] ?? ''),
                                );
                              }).toList(),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 15.0),
                            ),
                            RaisedButton(
                              child: Text('Add payee'),
                              onPressed: () async {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          SearchPayeeScreen()),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      Text(
                        'Available balance: ' +
                            NumberFormat.simpleCurrency(
                                    locale: 'id', decimalDigits: 0)
                                .format(balance),
                        style: TextStyle(fontSize: 15.0),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            hintText: 'Amount will be transfer',
                            // labelText: amountValidator()
                            labelText: 'Amount'),
                        keyboardType: TextInputType.number,
                        validator: validateAmount,
                        onSaved: (input) => amount = input,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            hintText: 'Description here',
                            labelText: 'Description'
                            // labelText: descValidator()
                            ),
                        // keyboardType: TextInputType.multiline,
                        validator: validateDesc,
                        onSaved: (input) => description = input,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15.0),
                      ),
                      Container(
                          width: 500.0,
                          child: Column(children: <Widget>[
                            DropdownButton(
                              hint: Text('Please select a category'),
                              value: category,
                              onChanged: (newValue) {
                                setState(() {
                                  category = newValue;
                                });
                              },
                              items: _categorys.map((location) {
                                return DropdownMenuItem(
                                  child: new Text(location),
                                  value: location,
                                );
                              }).toList(),
                            ),
                          ])),
                      Padding(
                        padding: EdgeInsets.only(top: 15.0),
                      ),
                      Visibility(
                          visible: _isVisible,
                          child: TextFormField(
                            obscureText: true,
                            decoration:
                                new InputDecoration(hintText: 'Your PIN'),
                            maxLength: 4,
                            onSaved: (input) => pin = input,
                          )),
                      RaisedButton(
                        child: Text('Send'),
                        onPressed: () {
                          _submitPayment();
                        },
                      ),

                      // TextFormField(
                      //   obscureText: true,
                      //   decoration: new InputDecoration(hintText: 'Your PIN'),
                      //   maxLength: 4,
                      //   onSaved: (input) => pin = input,
                      // ),
                      // RaisedButton(
                      //   child: Text('Pay'),
                      //   onPressed: () async {
                      //     await _submitPayment();
                      //   },
                      // ),
                      // buttonHandler()
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  String validateAmount(String value) {
    var valueInt = int.tryParse(value);
    if (valueInt == null) {
      return "Please enter the amount ";
      // } else if (valueInt < 1000 || valueInt > 1000) {
      //   return "Amount must be 1.000 - 100.000.000";
    } else if (valueInt >= balance) {
      return "Amount must not higher than your balance";
    } else {
      return null;
    }
  }

  String validateDesc(String value) {
    if (value == null) {
      return "Please enter the description";
    } else if (value.length >= 200) {
      return "Description must be under 200 char";
    } else {
      return null;
    }
  }
}
