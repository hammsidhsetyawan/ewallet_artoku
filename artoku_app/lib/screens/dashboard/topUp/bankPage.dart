import 'package:flutter/material.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:flutter/scheduler.dart';
import 'otpTopUpPage.dart';

class BankPage extends StatelessWidget {
  final appTitle = 'Bank Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      initialRoute: '/',
      home: MyHomePage(),
      routes: {
        '/topUpPage': (context) => OtpTopUpPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  final String title = "Bank Page";
  int value = 0;
  final myController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  Map data = {'name': String};

  // @override
  // void initState() {
  //   super.initState();
  //   bloc.otpReqUser();

  // }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    super.dispose();
    myController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _showSnackBar(context, String error) {
      final snackBar = SnackBar(content: Text(error));
      Scaffold.of(context).showSnackBar(snackBar);
    }

    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Center(
        child: Stack(fit: StackFit.expand, children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration:
                    InputDecoration(labelText: "Enter amount of top up"),
                keyboardType: TextInputType.number,
                controller: myController,
                onSaved: (input) => data['name'] = input,
              ),
              Builder(
                builder: (context) => RaisedButton(
                  child: Text("Submit"),
                  onPressed: () {
                    value = int.tryParse(myController.text);
                    if (value < 1000 || value > 100000000) {
                      return _showSnackBar(
                          context, "Enter amount between 1.000 - 100.000.000");
                    } else {
                      blocs.updateNavigation(myController.text);
                      bloc.otpReqUser();
                      // showDialog(
                      //   context: context,
                      //   builder: (context) {
                      //     return AlertDialog(
                      //       // Retrieve the text the user has entered by using the
                      //       // TextEditingController.
                      //       content: Text("Valid amount"),
                      //     );
                      //   },
                      // );
                      // return Navigator.pushNamed(context, '/topUpPage');
                    }
                  },
                  splashColor: Colors.blueAccent,
                ),
              ),
              Container(
                child: StreamBuilder(
                  stream: blocs.getNavigation,
                  initialData: blocs.navigationProvider.currentToken,
                  builder: (context, snapshot) {
                    // bool load = blocs.navigationProvider.currentLoad;
                    if (blocs.navigationProvider.currentFund ==
                        "insufficient funds") {
                      blocs.updateFundMessage("");
                      blocs.updateToken(0);
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        return _showSnackBar(context, "Insufficient Funds");
                      });
                      return Text("");
                    } else if ((blocs.navigationProvider.currentToken == 0) &&
                        (blocs.navigationProvider.currentFund == "")) {
                      return Text("");
                    } else {
                      SchedulerBinding.instance.addPostFrameCallback((_) {
                        Navigator.pushReplacementNamed(context, '/topUpPage');
                      });
                      return Text("");
                    }
                  },
                ),
              )
            ],
          ),
        ]),
      ),
    );
  }
}
