import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:flutter/material.dart';
import 'bankPage.dart';
import 'cardPage.dart';

class TopUpPage extends StatelessWidget {
  final appTitle = 'Top Up Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      home: MyHomePage(title: appTitle),
      routes: {
        '/bankPage': (context) => BankPage(),
        '/cardPage': (context) => CardPage(),
        
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Center(
        child: Stack(fit: StackFit.expand, children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.all(20.0),
                  child: Row(children: <Widget>[
                    RaisedButton(
                      child: Text('Bank'),
                      onPressed: () async {
                        blocs.updateSource("bank");
                        Navigator.pushReplacementNamed(context, '/bankPage');
                      },
                    ),
                  ])),
              Container(
                  margin: const EdgeInsets.all(20.0),
                  child: Row(children: <Widget>[
                    RaisedButton(
                      child: Text('Card'),
                      onPressed: () async {
                        blocs.updateSource("credit");
                        Navigator.pushNamed(context, '/cardPage');
                      },
                    ),
                  ]))
            ],
          )
        ]),
      ),
    );
  }
}