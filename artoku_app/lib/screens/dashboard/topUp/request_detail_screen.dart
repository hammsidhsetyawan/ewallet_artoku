import 'package:artoku_app/screens/dashboard/request_payment/respond_payment_screen.dart';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:artoku_app/src/models/payRequestModel.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class RequestDetailScreen extends StatelessWidget {
  final String email = blocs.navigationProvider.currentEmail;
  @override
  Widget build(BuildContext context) {
    PayRequest _payRequest = ModalRoute.of(context).settings.arguments;
    var payRequestDate = _payRequest.createdAt.toLocal();
    var formatter = new DateFormat('d MMM yyyy, HH:mm:ss');
    String formatted = formatter.format(payRequestDate);
    return Material(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        color: Colors.blueAccent,
        child: DraggableScrollableSheet(
          builder: (context, scrollController) {
            return Container(
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 24,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Payment Request Detail",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 26,
                                    color: Colors.black),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5),
                              ),
                              Text(
                                "Request ID:",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 18,
                                    color: Colors.grey),
                              ),
                              Text(
                                "${_payRequest.payRequestId}",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 19,
                                    color: Colors.grey),
                              ),
                            ],
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.close,
                              color: Colors.lightBlue[900],
                              size: 30,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          )
                        ],
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 32),
                    ),

                    SizedBox(
                      height: 16,
                    ),

                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 32),
                      child: Row(
                        children: <Widget>[
                          //copy same button
                          Container(
                            child: Text(
                              "$formatted",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16,
                                  color: Colors.grey[900]),
                            ),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey[200],
                                      blurRadius: 10.0,
                                      spreadRadius: 4.5)
                                ]),
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                          ),

                          SizedBox(
                            width: 16,
                          ),
                        ],
                      ),
                    ),

                    //Container for card
                    SizedBox(
                      height: 16,
                    ),

                    SizedBox(
                      height: 16,
                    ),

                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[100],
                                spreadRadius: 10.0,
                                blurRadius: 4.5)
                          ]),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.symmetric(horizontal: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.credit_card,
                                color: Colors.lightBlue[900],
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                "From",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey[700]),
                              )
                            ],
                          ),
                          Text(
                            "${_payRequest.useremail}",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: Colors.grey[700]),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[100],
                                spreadRadius: 10.0,
                                blurRadius: 4.5)
                          ]),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.symmetric(horizontal: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.credit_card,
                                color: Colors.lightBlue[900],
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                "To",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey[700]),
                              )
                            ],
                          ),
                          Text(
                            "${_payRequest.emailpayee}",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: Colors.grey[700]),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[100],
                                spreadRadius: 10.0,
                                blurRadius: 4.5)
                          ]),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.symmetric(horizontal: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.attach_money,
                                color: Colors.lightBlue[900],
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                "Amount",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey[700]),
                              )
                            ],
                          ),
                          Text(
                            NumberFormat.simpleCurrency(
                                    locale: 'id', decimalDigits: 0)
                                .format(_payRequest.amount),
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: Colors.grey[700]),
                          )
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 16,
                    ),

                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[100],
                                spreadRadius: 10.0,
                                blurRadius: 4.5)
                          ]),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.symmetric(horizontal: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.comment,
                                color: Colors.lightBlue[900],
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                "Description",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey[700]),
                              )
                            ],
                          ),
                          Text(
                            "${_payRequest.description}",
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: Colors.grey[700]),
                          )
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 16,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey[100],
                                spreadRadius: 10.0,
                                blurRadius: 4.5)
                          ]),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      margin: EdgeInsets.symmetric(horizontal: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.comment,
                                color: Colors.lightBlue[900],
                              ),
                              SizedBox(
                                width: 16,
                              ),
                              Text(
                                "Status",
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                    color: Colors.grey[700]),
                              )
                            ],
                          ),
                          Text("${_payRequest.status}",
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: _payRequest.status == 'completed'
                                    ? Colors.lightGreen
                                    : Colors.yellow[800],
                              ))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    _payRequest.status == "pending" &&
                            email == _payRequest.emailpayee
                        ? Container(
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RaisedButton(
                                      child: Text(
                                        "Respond",
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                RespondPaymentScreen(),
                                            // Pass the arguments as part of the RouteSettings. The
                                            // DetailScreen reads the arguments from these settings.
                                            settings: RouteSettings(
                                              arguments: _payRequest,
                                            ),
                                          ),
                                        );
                                      },
                                      color: Colors.green,
                                      textColor: Colors.white,
                                      padding:
                                          EdgeInsets.fromLTRB(50, 15, 50, 15),
                                      splashColor: Colors.grey,
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(30.0)))
                                ],
                              ),
                            ),
                          )
                        : Container()
                  ],
                ),
              ),
              decoration: BoxDecoration(
                  color: Color.fromRGBO(243, 245, 248, 1),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(40),
                      topLeft: Radius.circular(40))),
            );
          },
          initialChildSize: 0.95,
          maxChildSize: 0.95,
          minChildSize: 0.95,
        ),
      ),
    );
  }
}
