import 'package:flutter/material.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import "dart:async";
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import '../home_screen.dart';

class OtpTopUpPage extends StatelessWidget {
  final appTitle = 'OTP Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      initialRoute: '/',
      home: MyHomePage(),
      routes: {
        '/dashboardPage': (context) => HomeScreen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  final String title = "OTP Page";
  String pin = "";
  int value = 0;
  final myController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Future delay() async {
    await new Future.delayed(const Duration(seconds: 30));
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _showSnackBar(context, String error) {
      final snackBar = SnackBar(content: Text(error));
      Scaffold.of(context).showSnackBar(snackBar);
    }

    Future saveKey() async {
      return _formKey.currentState.save();
    }

    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: Center(
        child: Stack(fit: StackFit.expand, children: <Widget>[
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 120.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: TextFormField(
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        hintText: "Enter OTP",
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                      keyboardType: TextInputType.number,
                      onSaved: (String text) {
                        pin = text;
                        blocs.updatePin(text);
                        bloc.otpVerifyUser();
                      },
                    ),
                  ),
                  RaisedButton(
                    child: Text("Submit"),
                    onPressed: () async {
                      await saveKey();
                      print(pin);
                      delay();
                    },
                    splashColor: Colors.blueAccent,
                  ),
                  StreamBuilder(
                    stream: blocs.getNavigation,
                    builder: (context, snapshot) {
                      bool res = blocs.navigationProvider.currentMessage;
                      print("DARI PAGE" + res.toString());
                      if (res == true) {
                        bloc.updateBalanceUser();
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          _showSnackBar(context, "Success");
                          Navigator.pushReplacementNamed(
                              context, '/dashboardPage');
                        });
                        return Text("");
                      } else if (res == null) {
                        return Text("");
                      } else {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          return _showSnackBar(context, "Failed");
                        });
                        blocs.updateOtpMessage(null);
                        return Text("");
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
