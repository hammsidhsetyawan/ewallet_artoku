import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:artoku_app/src/blocs/transactionBloc.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:artoku_app/src/models/transactionModel.dart';
import 'package:artoku_app/screens/dashboard/transaction_detail_screen.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TransactionHistory extends StatefulWidget {
  TransactionHistory({Key key}) : super(key: key);
  @override
  TransactionHistoryState createState() => TransactionHistoryState();
}

class TransactionHistoryState extends State<TransactionHistory> {
  var yearNow = new DateTime.now().year;
  String year = 'All';
  bool selected;
  Future delay() async {
    await new Future.delayed(const Duration(seconds: 10));
  }

  @override
  Widget build(BuildContext context) {
    bloc.getBalanceUser();
    TransactionBloc transactionHistoryBloc = TransactionBloc();
    delay();
    Stream transactionList = transactionHistoryBloc.transactionListAll;
    int balance = blocs.navigationProvider.currentBalance;
    return Scaffold(
      body: Container(
        color: Colors.blueAccent,
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            //Container for top data
            Padding(
              padding:
                  EdgeInsets.only(top: 85, left: 100, right: 50, bottom: 80),
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 32, vertical: 32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          NumberFormat.simpleCurrency(
                                  locale: 'id', decimalDigits: 0)
                              .format(balance),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text(
                        "Available Balance",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 16,
                            color: Colors.blue[100]),
                      ),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                  ],
                ),
              ),
            ),
            //draggable sheet
            DraggableScrollableSheet(
              builder: (context, scrollController) {
                return Container(
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(243, 245, 248, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40))),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 24,
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "All Transactions",
                                style: TextStyle(
                                    fontWeight: FontWeight.w900,
                                    fontSize: 24,
                                    color: Colors.black),
                              ),
                              dropdownYear(),
                            ],
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 25),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        StreamBuilder<List<Transaction>>(
                          stream: transactionList,
                          builder: (BuildContext context,
                              AsyncSnapshot<List<Transaction>> snapshot) {
                            if (snapshot.hasData) {
                              return buildTransactionHistoryList(snapshot);
                            } else if (snapshot.hasError) {
                              return Text(snapshot.error.toString());
                            }
                            return Center(child: CircularProgressIndicator());
                          },
                        ),
                      ],
                    ),
                    controller: scrollController,
                  ),
                );
              },
              initialChildSize: 0.65,
              minChildSize: 0.65,
              maxChildSize: 1,
            )
          ],
        ),
      ),
    );
  }

  Widget dropdownYear() {
    List<String> listYear = ["All"];
    for (int i = yearNow; i >= yearNow - 3; i--) {
      listYear.add(i.toString());
    }
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: Container(
            margin: EdgeInsets.only(top: 20, left: 40, right: 10),
            padding: const EdgeInsets.only(left: 10),
            width: 120.0,
            height: 50.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(width: 3, color: Colors.blue[500])),
            child: DropdownButton(
              value: year,
              onChanged: (newValue) {
                bloc.filterHistory();
                setState(() {
                  year = newValue;
                  selected = true;
                });
              },
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
              items: listYear.map((yearList) {
                return DropdownMenuItem(
                  child: new Text(
                    yearList.toString() ?? "All",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                  value: yearList,
                );
              }).toList(),
            ),
          ),
        )
      ],
    );
  }

  Widget buildTransactionHistoryList(AsyncSnapshot snapshot) {
    List _list = snapshot.data
        .where((transaction) =>
            year == 'All' || transaction.createdAt.year.toString() == year)
        .toList();
    if (_list.length == 0) {
      return Align(
        alignment: Alignment.center,
        child: Container(
          height: 200,
          margin: EdgeInsets.only(top: 100),
          child: Text(
            "It's so lonely, there's nothing here.",
            // textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500, fontSize: 15, color: Colors.black),
          ),
        ),
      );
    } else
      return ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int index) {
          print(_list.length);
          Transaction _transaction = _list[index];
          var transactionDate = _transaction.createdAt;
          var formatter = new DateFormat('dd-MM-yyyy');
          String formatted = formatter.format(transactionDate);
          return InkWell(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 32),
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.all(Radius.circular(18))),
                    child: Icon(
                      _transaction.type == 'debit'
                          ? Icons.arrow_upward
                          : Icons.arrow_downward,
                      color: _transaction.type != 'debit'
                          ? Colors.lightGreen
                          : Colors.red[400],
                    ),
                    padding: EdgeInsets.all(12),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          _transaction.type == 'debit'
                              ? "Outgoing Transfer"
                              : _transaction.type == 'credit'
                                  ? "Incoming Transfer"
                                  : "Top Up",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[900]),
                        ),
                        Text(
                          _transaction.type == 'debit'
                              ? "Transfer to " + _transaction.payee
                              : _transaction.type == 'credit'
                                  ? "Transfer from " + _transaction.payee
                                  : "Top Up from " + _transaction.payee,
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[500]),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        _transaction.type != 'debit'
                            ? NumberFormat.simpleCurrency(
                                    locale: 'id', decimalDigits: 0)
                                .format(_transaction.amount)
                            : NumberFormat.simpleCurrency(
                                    locale: 'id', decimalDigits: 0)
                                .format(_transaction.amount),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: _transaction.type != 'debit'
                              ? Colors.lightGreen
                              : Colors.red[400],
                        ),
                      ),
                      Text(
                        "$formatted",
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[500]),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => TransactionDetailScreen(),
                  // Pass the arguments as part of the RouteSettings. The
                  // DetailScreen reads the arguments from these settings.
                  settings: RouteSettings(
                    arguments: _transaction,
                  ),
                ),
              );
            },
          );
        },
        shrinkWrap: true,
        padding: EdgeInsets.all(0),
        controller: ScrollController(keepScrollOffset: false),
      );
  }
}
