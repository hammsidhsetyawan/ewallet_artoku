import 'package:artoku_app/screens/session/signup_screen.dart';
import 'package:artoku_app/screens/dashboard/home_screen.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../src/blocs/navigation_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    super.initState();
    autoLogIn();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool _showPassword = false;
  bool _triggerSnack = false;
  String _useremail, _password;
  String _passwordDigest;
  String _responseMessage;

  void autoLogIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String user = prefs.getString('useremail');
    print('sharedprefs $user');
    if (user != null) {
      setState(() {
        _useremail = user;
      });
      blocs.updateEmail(user);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
      print("autoLogIn success");
    }
  }

  Future _submit() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      final _byte = utf8.encode(_password);
      _passwordDigest = md5.convert(_byte).toString();
      final _response = await bloc.loginUser(_useremail, _passwordDigest);
      _responseMessage = _response["message"];
      print(_responseMessage);
      if (_response["message"] == 'Login success') {
        await loginUser();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => HomeScreen()));
      } else if (_response["message"] != 'Login success') {
        setState(() => _triggerSnack = true);
      }
      // print('snack $_triggerSnack');
    } else {
      setState(() => _validate = true);
    }
  }

  Future<Null> loginUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('useremail', _useremail);
    print('Store Login: ${prefs.getString('useremail')}');
  }

  Future delay() async {
    await new Future.delayed(const Duration(seconds: 20));
  }

  @override
  Widget build(BuildContext context) {
    _showSnackBar(context, String error) {
      final snackBar = SnackBar(content: Text(error));
      Scaffold.of(context).showSnackBar(snackBar);
    }

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        // padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Form(
                key: _formKey,
                autovalidate: _validate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      'Log in to ArtoKu',
                      style: TextStyle(fontSize: 26.0),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Your registered e-mail',
                        labelText: 'E-mail',
                        prefixIcon: Icon(Icons.email),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: validateEmail,
                      onSaved: (String value) {
                        print(value);
                        _useremail = value;
                        blocs.updateEmail(value);
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                          hintText: 'Your super secret password',
                          labelText: 'Password',
                          prefixIcon: Icon(Icons.vpn_key),
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.remove_red_eye,
                              color: this._showPassword
                                  ? Colors.blue
                                  : Colors.grey,
                            ),
                            onPressed: () {
                              setState(() =>
                                  this._showPassword = !this._showPassword);
                            },
                          )),
                      obscureText: !this._showPassword,
                      validator: validatePassword,
                      onSaved: (String value) {
                        _password = value;
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                    Builder(
                      builder: (context) => RaisedButton(
                        child: Text('Log In'),
                        onPressed: () async {
                          await _submit();
                          _responseMessage != 'Login success' &&
                                  _responseMessage != null
                              ? _showSnackBar(context, _responseMessage)
                              : setState(() => _triggerSnack = false);
                          print(_triggerSnack);
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Row(
                      children: <Widget>[
                        Text("Not yet registered? "),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignupScreen()),
                            );
                          },
                          child: Text(
                            "Register here!",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}

String validateEmail(String value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (value.length == 0) {
    return "Email is required";
  } else if (!regex.hasMatch(value)) {
    return "Invalid Email";
  } else {
    return null;
  }
}

String validatePassword(String value) {
  if (value.length == 0) {
    return "Password is required";
  } else if (value.length < 6) {
    return "Password must be 6 character minimum";
  } else {
    return null;
  }
}
