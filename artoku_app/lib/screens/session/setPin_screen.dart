import 'package:flutter/material.dart';
import 'package:artoku_app/src/blocs/userBloc.dart';
import 'login_screen.dart';

class SetPinScreen extends StatefulWidget {
  final String value;
  SetPinScreen({Key key, this.value}) : super(key: key);
  @override
  _SetPinScreenState createState() => _SetPinScreenState();
}

class _SetPinScreenState extends State<SetPinScreen> {
  GlobalKey<FormState> _key = new GlobalKey();
  bool _validate = false;
  String pin, pinConfirmation, useremail;
  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    return Scaffold(
      body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
                // margin: new EdgeInsets.all(15.0),
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Form(
                      key: _key,
                      autovalidate: _validate,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                              obscureText: true,
                              decoration:
                                  InputDecoration(hintText: 'Create a new pin'),
                              maxLength: 4,
                              validator: validatePin,
                              onChanged: (text) => setState(() {
                                    pin = text;
                                  })),
                          TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                                hintText: 'Confirmation your pin',
                                helperText: _validate ? 'Pin matched!' : '',
                                helperStyle: TextStyle(color: Colors.green)),
                            maxLength: 4,
                            onChanged: (text) => setState(() {
                              pinConfirmation = text;
                            }),
                            validator: validatePinConfirmation,
                          ),
                          SizedBox(height: 15.0),
                          RaisedButton(
                            onPressed: () async {
                              if (_key.currentState.validate()) {
                                var useremail = widget.value;
                                bloc.addSetPin(useremail, pin);
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginScreen()));
                              } else {
                                setState(() {
                                  _validate = true;
                                });
                              }
                            },
                            child: Text('Submit'),
                          )
                        ],
                      )),
                ]),
          )),
    );
  }

  String validatePin(String value) {
    String patttern = r'^[a-zA-Z0-9]+$';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Pin is required!";
    } else if (value.length < 4) {
      return "Pin must be 4 characters";
    } else if (!regExp.hasMatch(value)) {
      return "Please enter Alphanumeric Pin";
    }
    return null;
  }

  String validatePinConfirmation(String value) {
    if (pin != pinConfirmation) {
      return "Your pin is not matched! Please check again.";
    } else if (value.length == 0) {
      return "Pin is required!";
    } else if (value.length < 4) {
      return "Pin must be 4 characters";
    }
    return null;
  }
}
