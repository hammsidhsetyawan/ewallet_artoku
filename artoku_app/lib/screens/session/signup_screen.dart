// import '../../src/models/response/session/session_response.dart';
import 'package:artoku_app/screens/session/setPin_screen.dart';
import 'package:flutter/material.dart';
import '../../src/blocs/userBloc.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _firstname,
      _lastname,
      _username,
      _address,
      _useremail,
      _password,
      _passwordConfirmation,
      _passwordDigest;

  var _useremailController = new TextEditingController();
  String _responseMessage;
  bool _triggerSnack = false;
  bool _validate = false;
  bool _showPassword = false;
  bool _showPasswordConfirmation = false;

  void _signUpUser() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      final _byte = utf8.encode(_password);
      _passwordDigest = md5.convert(_byte).toString();

      if (_password != _passwordConfirmation) {
        print('password not same');
      } else {
        final _response = await bloc.createNewUser(_firstname, _lastname,
            _username, _address, _useremail, _passwordDigest);
        _responseMessage = _response["message"];
        // print(_response["message"]);
        if (_response["message"] == 'Signup success') {
          var route = new MaterialPageRoute(
            builder: (BuildContext context) =>
                new SetPinScreen(value: _useremailController.text),
          );
          Navigator.of(context).push(route);
        } else if (_response["message"] != 'Signup success') {
          setState(() => _triggerSnack = true);
        }
      }
    } else {
      setState(() => _validate = true);
    }
  }

  @override
  Widget build(BuildContext context) {
    _showSnackBar(context, String error) {
      final snackBar = SnackBar(content: Text(error));
      Scaffold.of(context).showSnackBar(snackBar);
    }

    return Scaffold(
        body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Form(
                    key: _formKey,
                    autovalidate: _validate,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          'Sign Up to Artoku',
                          style: TextStyle(fontSize: 26.0),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 30.0),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Firstname',
                              hintText: 'Your firstname'),
                          validator: (input) => input.trim().isEmpty
                              ? 'Please enter a valid name'
                              : null,
                          onSaved: (input) => _firstname = input,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Lastname', hintText: 'Your lastname'),
                          validator: (input) => input.trim().isEmpty
                              ? 'Please enter a valid name'
                              : null,
                          onSaved: (input) => _lastname = input,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Username', hintText: 'Your username'),
                          validator: (input) => input.trim().isEmpty
                              ? 'Please enter a valid name'
                              : null,
                          onSaved: (input) => _username = input,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Address', hintText: 'Your address'),
                          validator: (input) => input.trim().isEmpty
                              ? 'Please enter a valid address'
                              : null,
                          onSaved: (input) => _address = input,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Email', hintText: 'Your email'),
                          controller: _useremailController,
                          keyboardType: TextInputType.emailAddress,
                          validator: validateEmail,
                          onSaved: (value) => _useremail = value,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Password',
                              hintText: 'Your password',
                              suffixIcon: IconButton(
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: this._showPassword
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                onPressed: () {
                                  setState(() =>
                                      this._showPassword = !this._showPassword);
                                },
                              )),
                          validator: validatePassword,
                          onSaved: (value) => _password = value,
                          obscureText: !this._showPassword,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                              labelText: 'Password confirmation',
                              hintText: 'Password confirmation',
                              // helperText: _validate ? 'Password matched!' : '',
                              // helperStyle: TextStyle(color: Colors.green),
                              suffixIcon: IconButton(
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: this._showPasswordConfirmation
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                onPressed: () {
                                  setState(() =>
                                      this._showPasswordConfirmation =
                                          !this._showPasswordConfirmation);
                                },
                              )),
                          validator: validatePasswordConfirmation,
                          onSaved: (value) => _passwordConfirmation = value,
                          obscureText: !this._showPasswordConfirmation,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 30.0),
                        ),
                        Builder(
                            builder: (context) => RaisedButton(
                                  child: Text('Sign Up'),
                                  onPressed: () async {
                                    _signUpUser();
                                    _responseMessage != 'Signup success' &&
                                            _responseMessage != null
                                        ? _showSnackBar(
                                            context, _responseMessage)
                                        : setState(() => _triggerSnack = false);
                                    print(_triggerSnack);
                                    // _triggerSnack == true
                                    //     ? _showSnackBar(
                                    //         context, _responseMessage)
                                    //     : setState(() => _triggerSnack = false);
                                  },
                                ))
                      ],
                    ),
                  )
                ],
              ),
            )));
  }

  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is required";
    } else if (!regex.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }

  String validatePassword(String value) {
    if (value.length == 0) {
      return "Password is required";
    } else if (value.length < 6) {
      return "Password must be 6 character minimum";
    } else {
      return null;
    }
  }

  String validatePasswordConfirmation(String value) {
    if (_password != _passwordConfirmation) {
      return "Your password is not matched! Please check again.";
    } else if (value.length == 0) {
      return "password is required!";
    }
    return null;
  }
}
