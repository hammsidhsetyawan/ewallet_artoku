// import 'package:artoku_app/src/models/todoModels.dart';
import 'package:artoku_app/src/models/payeeModels.dart';
import 'package:artoku_app/src/resources/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:artoku_app/src/models/UsersModels.dart';
// import '../resources/repository.dart';

class UserBloc {
  final _repository = Repository();
  final _userFetcher = PublishSubject<List<Users>>();
  final _useremail = BehaviorSubject<String>();
  // final _amount = BehaviorSubject<String>();
  final _id = BehaviorSubject<String>();

  Observable<List<Users>> get fetchUser => _userFetcher.stream;
  Function(String) get updateUserEmail => _useremail.sink.add;
  Function(String) get getId => _id.sink.add;

  Stream<List<Users>> get payeeList async* {
    yield await _repository.fetchPayee();
  }

  createNewUser(
      String firstname, lastname, username, address, useremail, password) {
    final response = _repository.createNewUser(
        firstname, lastname, username, address, useremail, password);
    return response;
  }

  loginUser(String useremail, password) {
    print("userbloc email: $useremail");
    print("userbloc passwordDigest: $password");
    final response = _repository.loginUser(useremail, password);
    return response;
  }

  addSetPin(String useremail, pinUser) {
    // _pinUser.value = pinUser;
    print('pin user value : $pinUser');
    final response = _repository.addSetPin(useremail, pinUser);
    return response;
  }

  fetchAllUsers() async {
    List<Users> users = await _repository.fetchAllUsers();
    _userFetcher.sink.add(users);
  }

  updateBalanceUser() {
    _repository.updateBalance();
  }

  addRequest() {
    _repository.requestPayment();
  }
  // updateTransactionType(){
  //   _repository.updateTransactionType();
  // }

  getBalanceUser() {
    _repository.getBalance();
  }

  Stream get getStreamBalanceUser async* {
    yield await _repository.getBalance();
  }

  otpReqUser() {
    _repository.otpReq();
  }

  otpVerifyUser() {
    _repository.otpVerify();
  }

  addPayeeUser(String useremail, username) {
    _repository.addPayee(useremail, username);
  }

  filterHistory() {
    _repository.fetchHistoryTransactions();
  }

  dispose() {
    _useremail.close();
    _id.close();
    _userFetcher.close();
  }
}

final bloc = UserBloc();
