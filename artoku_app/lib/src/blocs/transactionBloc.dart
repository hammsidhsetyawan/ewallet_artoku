import 'package:artoku_app/src/models/transactionModel.dart';
import 'package:artoku_app/src/resources/repository.dart';

class TransactionBloc {
  final _repository = Repository();
  Stream<List<Transaction>> get transactionListNow async* {
    yield await _repository.fetchTransactions();
  }

  addPayment(payee, amount, category, description, pinUser) {
    final response =
        _repository.addPayment(payee, amount, category, description, pinUser);
    return response;
  }

  dispose() {}

  Stream<List<Transaction>> get transactionListAll async* {
    yield await _repository.fetchHistoryTransactions();
  }
}

final transBloc = TransactionBloc();
