import 'dart:async';

import 'package:artoku_app/src/blocs/navigation_provider.dart';
import 'package:rxdart/rxdart.dart';

class NavigationDrawerBloc {
  final navigationController = BehaviorSubject();
  NavigationProvider navigationProvider = new NavigationProvider();

  Stream get getNavigation => navigationController.stream;
  Stream get getBalance => navigationController.stream;

  void updateNavigation(String navigation) {
    navigationProvider.updateNavigation(navigation);
    navigationController.sink.add(navigationProvider.currentNavigation);
  }

  void updateToken(int token) {
    navigationProvider.updateToken(token);
    navigationController.sink.add(navigationProvider.currentToken);
  }

  void updateLoad(bool load) {
    navigationProvider.updateLoad(load);
    navigationController.sink.add(navigationProvider.currentLoad);
  }

  void updateFundMessage(String message) {
    navigationProvider.updateFund(message);
    navigationController.sink.add(navigationProvider.currentFund);
  }

  void updateBalance(int balance) {
    navigationProvider.updateBalance(balance);
    navigationController.sink.add(navigationProvider.currentBalance);
  }

  void updateAmountReq(int amount) {
    navigationProvider.updateAmountReq(amount);
    navigationController.sink.add(navigationProvider.currentAmountReq);
  }

  void updateEmailReq(String email) {
    navigationProvider.updateEmailReq(email);
    navigationController.sink.add(navigationProvider.currentEmailReq);
  }

  void updateDescriptionReq(String description) {
    navigationProvider.updateDescriptionReq(description);
    navigationController.sink.add(navigationProvider.currentDescriptionReq);
  }

  void updatePin(String pin) {
    navigationProvider.updatePin(pin);
    navigationController.sink.add(navigationProvider.currentPin);
  }

  void updateEmail(String email) {
    navigationProvider.updateEmail(email);
    navigationController.sink.add(navigationProvider.currentEmail);
  }

  void updateType(String type) {
    navigationProvider.updateType(type);
    navigationController.sink.add(navigationProvider.currentType);
  }

  void updateSource(String source) {
    navigationProvider.updateSource(source);
    navigationController.sink.add(navigationProvider.currentSource);
  }

  void updateOtpMessage(bool message) {
    navigationProvider.updateMessage(message);
    navigationController.sink.add(navigationProvider.currentMessage);
  }

  void dispose() {
    navigationController.close();
  }
}

final blocs = NavigationDrawerBloc();
