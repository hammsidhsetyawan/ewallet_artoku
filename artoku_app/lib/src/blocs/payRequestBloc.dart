import 'package:artoku_app/src/models/payRequestModel.dart';
import 'package:artoku_app/src/resources/repository.dart';

class PayRequestBloc {
  final _repository = Repository();
  Stream<List<PayRequest>> get receivedPayRequestListNow async* {
    yield await _repository.fetchReceivedPayRequests();
  }

  Stream<List<PayRequest>> get sentPayRequestListNow async* {
    yield await _repository.fetchSentPayRequests();
  }

  addPayment(payee, amount, category, description, pinUser) {
    final response =
        _repository.addPayment(payee, amount, category, description, pinUser);
    return response;
  }

  dispose() {}
}

final payReqBloc = PayRequestBloc();
