class NavigationProvider {
  String currentNavigation = "Home";
  int currentToken = 0;
  int currentAmountReq = 0;
  String currentPin = "";
  String currentEmail = "";
  String currentType= "";
  String currentSource= "";
  String currentEmailReq= "";
  String currentDescriptionReq= "";
  bool currentMessage = false;
  int currentBalance = 0;
  String currentFund = "";
  bool currentLoad = false;

  void updateNavigation(String navigation) {
    currentNavigation = navigation;
  }

  void updateEmailReq(String email) {
    currentEmailReq = email;
  }

  void updateDescriptionReq(String description) {
    currentDescriptionReq = description;
  }

  void updateToken(int token) {
    currentToken = token;
  }

  void updateLoad(bool load) {
    currentLoad = load;
  }

  void updateBalance(int balance) {
    currentBalance = balance;
  }

  void updateAmountReq(int amount) {
    currentAmountReq = amount;
  }

  void updateFund(String message) {
    currentFund = message;
  }

   void updateType(String type) {
    currentType= type;
  }

  void updateSource(String source) {
    currentSource= source;
  }

  void updatePin(String pin) {
    currentPin = pin;
  }

  void updateEmail(String email) {
    currentEmail = email;
  }

  void updateMessage(bool message) {
    currentMessage = message;
  }
}
