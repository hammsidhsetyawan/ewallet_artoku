import 'package:artoku_app/src/models/UsersModels.dart';
import 'package:flutter/foundation.dart';
import 'dart:async';
import 'dart:convert';
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import '../config/const.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:http/http.dart' as http;

class UserApiProvider {
  Client client = Client();

  Future createNewUser(String firstname, String lastname, String username,
      String address, String useremail, String password) async {
    Response response;
    response = await client.post("$base_url/signup", body: {
      "firstname": firstname,
      "lastname": lastname,
      "username": username,
      "address": address,
      "useremail": useremail,
      "password": password
    });

    print(response.body);
    if (response.statusCode == 200) {
      // print('signup succeeded');
      return json.decode(response.body);
    } else if (response.statusCode == 401) {
      // print('signup failed');
      return json.decode(response.body);
    } else if (response.statusCode == 402) {
      // print('Username already used');
      return json.decode(response.body);
    } else if (response.statusCode == 403) {
      // print('Email already used');
      return json.decode(response.body);
    } else {
      throw Exception('Failed to post');
    }
  }

  Future postLogin(String useremail, String password) async {
    print(useremail);
    print(password);
    Response response;
    response = await client.post("$base_url/login", body: {
      "useremail": useremail,
      "password": password,
    });
    print(response.body);
    if (response.statusCode == 200) {
      print('login succeeded');
      return json.decode(response.body);
    } else if (response.statusCode == 401) {
      print('login failed');
      return json.decode(response.body);
    } else if (response.statusCode == 404) {
      print('user not registered');
      return json.decode(response.body);
    } else {
      throw Exception('Failed to post');
    }
  }

  Future addPin(useremail, pinUser) async {
    print(useremail);
    print(pinUser);
    final response = await client.put("$base_url/setpinuser/$useremail",
        body: {'useremail': useremail, 'pinUser': pinUser});
    print('body: [${response.body}]');
    if (response.statusCode == 200) {
      print('berhasil di update');
      return response;
    } else {
      throw Exception('Failed to update data');
    }
  }

  Future otpReq() async {
    String amount = blocs.navigationProvider.currentNavigation;
    String email = blocs.navigationProvider.currentEmail;
    bool load = blocs.navigationProvider.currentLoad;
    blocs.updateLoad(true);
    print("LOAD "+load.toString());
    print('request otp for topup $amount to $email');
    final response = await client.post(
        "https://ewalletapis.herokuapp.com/topup",
        body: {"useremail": email, "amount": amount});
    Map<String, dynamic> responseJson = await json.decode(response.body);
    print(json.decode(response.body));
    blocs.updateToken(responseJson["token"]);
    int token = blocs.navigationProvider.currentToken;
    print(token.toString());
    // print(json.decode(response.body));
    // print(responseJson["message"]);
    await blocs.updateFundMessage(responseJson["message"]);

    String message = blocs.navigationProvider.currentFund;
    print("MESSAGE: $message");
    if (response.statusCode == 200) {
      return response;
    } else {
      throw Exception('Failed to add data');
    }
  }

  Future updateBalance() async {
    String amount = blocs.navigationProvider.currentNavigation;
    String email = blocs.navigationProvider.currentEmail;
    String type = blocs.navigationProvider.currentType;
    String source = blocs.navigationProvider.currentSource;
    final response = await client.put("$base_url/balances/$email", body: {
      "useremail": email,
      "amount": amount,
      "type": type,
      "source": source,
    });

    if (response.statusCode == 200) {
      return response;
    } else {
      throw Exception('Failed to add data');
    }
  }

  Future getBalance() async {
    String email = blocs.navigationProvider.currentEmail;
    final response = await client.get("$base_url/balances/$email");
    Map<String, dynamic> responseJson = await json.decode(response.body);
    blocs.updateBalance(responseJson["balance"]);
    int balance = blocs.navigationProvider.currentBalance;
    print("NEW BALANCE: " + balance.toString());
    print("updated balance: " + responseJson["balance"].toString());
    if (response.statusCode == 200) {
      return responseJson["balance"];
    } else {
      throw Exception('Failed to add data');
    }
  }

  Future otpVerify() async {
    int token = blocs.navigationProvider.currentToken;
    String pin = blocs.navigationProvider.currentPin;
    print(pin);
    final response = await client.post(
        "https://ewalletapis.herokuapp.com/verifytopup",
        body: {"token": token.toString(), "pin": pin});
    Map<String, dynamic> responseJson = await json.decode(response.body);
    print("DARI PROVIDER"+responseJson["success"].toString());
    blocs.updateOtpMessage(responseJson["success"]);
    bool res = blocs.navigationProvider.currentMessage;
    print("DARI PROVIDER"+res.toString());
    if (response.statusCode == 200) {
      return response;
    } else {
      throw Exception('Failed to add data');
    }
  }

  Future<List<Users>> fetchUserList() async {
    print('panggil data');
    final response = await client.get("$base_url/find");
    if (response.statusCode == 200) {
      // print(response.body.length);
      return compute(usersFromJson, response.body);

      // return ItemModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to Load');
    }
  }

  Future addPayee(useremail, username) async {
    print(useremail);
    print(username);
    String email = blocs.navigationProvider.currentEmail;

    await client.put("$base_url/addpayee/$email",
        body: {"useremail": useremail, "username": username});
  }

  Future fetchPayee() async {
    String email = blocs.navigationProvider.currentEmail;

    http.Response response = await http.get("$base_url/payee/$email");

    String content = response.body;
    final payee = usersFromJson(content);
    return payee;
  }
}
