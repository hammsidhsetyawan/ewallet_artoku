import 'dart:async';
import 'dart:convert';

import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:artoku_app/src/models/payRequestModel.dart';
import 'package:http/http.dart' as http;
import 'package:artoku_app/src/config/const.dart';

class RequestApiProvider {
  Future requestPayment() async {
    String email = blocs.navigationProvider.currentEmail;
    String emailPayee = blocs.navigationProvider.currentEmailReq;
    int amount = blocs.navigationProvider.currentAmountReq;
    String description = blocs.navigationProvider.currentDescriptionReq;
    final response = await http.post("$base_url/request", body: {
      "useremail": email,
      "amount": amount.toString(),
      "description": description,
      "emailpayee": emailPayee
    });
    print(email);
    Map<String, dynamic> responseJson = await json.decode(response.body);
    blocs.updateOtpMessage(responseJson["success"]);
    if (response.statusCode == 200) {
      return response;
    } else {
      throw Exception('Failed to add data');
    }
  }

  Future fetchSentPayRequests() async {
    String email = blocs.navigationProvider.currentEmail;

    http.Response response = await http.get("$base_url/request/$email/sent");
    await Future.delayed(Duration(milliseconds: 500));

    String content = response.body;

    final payRequest = payRequestFromJson(content);
    return payRequest;
  }

  Future fetchReceivedPayRequests() async {
    String email = blocs.navigationProvider.currentEmail;

    http.Response response =
        await http.get("$base_url/request/$email/received");
    await Future.delayed(Duration(milliseconds: 500));

    String content = response.body;

    final payRequest = payRequestFromJson(content);
    return payRequest;
  }
}
