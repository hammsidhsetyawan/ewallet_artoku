import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:artoku_app/src/blocs/navigation_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:artoku_app/src/config/const.dart';
import 'package:artoku_app/src/models/transactionModel.dart';
import 'dart:convert';

class TransactionApiProvider {
  Client client = Client();

  Future fetchTransactions() async {
    String email = blocs.navigationProvider.currentEmail;

    http.Response response =
        await http.get("$base_url/transaction/$email/history/5");
    await Future.delayed(Duration(milliseconds: 500));

    String content = response.body;

    final transaction = transactionFromJson(content);
    return transaction;
  }

  Future updateTransactionType() async {
    String email = blocs.navigationProvider.currentEmail;

    http.Response response =
        await http.get("$base_url/transaction/$email/history/5");
    await Future.delayed(Duration(milliseconds: 500));

    String content = response.body;

    final transaction = transactionFromJson(content);
    return transaction;
  }

  Future addPayment(payee, amount, category, description, pinUser) async {
    String useremail = blocs.navigationProvider.currentEmail;
    print("$useremail send money to $payee $amount rupiah");
    final response = await client.post("$base_url/payment", body: {
      "useremail": useremail,
      "payee": payee,
      "amount": amount,
      "category": category,
      "description": description,
      "pinUser": pinUser
    });
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to add data');
    }
  }

  Future fetchHistoryTransactions() async {
    String email = blocs.navigationProvider.currentEmail;
    http.Response response = await http.get("$base_url/transaction/$email");
    await Future.delayed(Duration(milliseconds: 500));
    String content = response.body;
    final transactionHistory = transactionFromJson(content);
    return transactionHistory;
  }
}
