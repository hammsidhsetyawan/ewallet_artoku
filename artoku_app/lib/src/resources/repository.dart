import 'dart:async';

import 'package:artoku_app/src/models/UsersModels.dart';
import 'package:artoku_app/src/resources/transactionApiProvider.dart';
import 'package:artoku_app/src/resources/userApiProvider.dart';
import 'package:artoku_app/src/resources/requestApiProvider.dart';

class Repository {
  // final title = '';
  final userApiProvider = UserApiProvider();
  final requestApiProvider = RequestApiProvider();
  final transactionApiProvider = TransactionApiProvider();

  // Future addSaveUsers(String useremail) => userApiProvider.addUser(useremail);
  Future updateBalance() => userApiProvider.updateBalance();
  Future requestPayment() => requestApiProvider.requestPayment();
  // Future updateTransactionType() => user.updateTransactionType();
  Future getBalance() => userApiProvider.getBalance();
  Future otpReq() => userApiProvider.otpReq();
  Future otpVerify() => userApiProvider.otpVerify();
  Future createNewUser(
          firstname, lastname, username, address, useremail, password) =>
      userApiProvider.createNewUser(
          firstname, lastname, username, address, useremail, password);
  Future loginUser(useremail, password) =>
      userApiProvider.postLogin(useremail, password);
  Future addSetPin(useremail, pinUser) =>
      userApiProvider.addPin(useremail, pinUser);
  Future<List<Users>> fetchAllUsers() => userApiProvider.fetchUserList();
  Future addPayee(useremail, username) =>
      userApiProvider.addPayee(useremail, username);
  Future fetchPayee() => userApiProvider.fetchPayee();
  Future fetchTransactions() => transactionApiProvider.fetchTransactions();
  Future fetchHistoryTransactions() =>
      transactionApiProvider.fetchHistoryTransactions();
  Future addPayment(payee, amount, category, description, pinUser) =>
      transactionApiProvider.addPayment(
          payee, amount, category, description, pinUser);
  Future fetchReceivedPayRequests() =>
      requestApiProvider.fetchReceivedPayRequests();
  Future fetchSentPayRequests() => requestApiProvider.fetchSentPayRequests();
}
