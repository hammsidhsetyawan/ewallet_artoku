// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

List<User> userFromJson(String str) =>
    List<User>.from(json.decode(str).map((x) => User.fromJson(x)));

String userToJson(List<User> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  bool auth;
  UserClass user;
  String message;
  
  User({
    this.auth,
    this.user,
    this.message,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        auth: json["auth"],
        user: UserClass.fromJson(json["user"]),
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "auth": auth,
        "user": user.toJson(),
        "message": message,
      };
}

class UserClass {
  String id;
  String firstname;
  String lastname;
  String username;
  String address;
  String useremail;
  String password;
  int userId;
  int failedAttempt;
  DateTime lockedUntil;
  int v;

  UserClass({
    this.id,
    this.firstname,
    this.lastname,
    this.username,
    this.address,
    this.useremail,
    this.password,
    this.userId,
    this.failedAttempt,
    this.lockedUntil,
    this.v,
  });

  factory UserClass.fromJson(Map<String, dynamic> json) => UserClass(
        id: json["_id"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        username: json["username"],
        address: json["address"],
        useremail: json["useremail"],
        password: json["password"],
        userId: json["id"],
        failedAttempt: json["failed_attempt"],
        lockedUntil: DateTime.parse(json["locked_until"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        'firstname': firstname,
        'lastname': lastname,
        'username': username,
        'address': address,
        "useremail": useremail,
        "password": password,
        "id": userId,
        "failed_attempt": failedAttempt,
        "locked_until": lockedUntil.toIso8601String(),
        "__v": v,
      };
}
