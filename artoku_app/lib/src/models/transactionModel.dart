// To parse this JSON data, do
//
//     final transaction = transactionFromJson(jsonString);
import 'dart:convert';

List<Transaction> transactionFromJson(String str) => List<Transaction>.from(
    json.decode(str).map((x) => Transaction.fromJson(x)));
String transactionToJson(List<Transaction> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Transaction {
  String id;
  String transactionId;
  String useremail;
  String payee;
  int amount;
  int year;
  String category;
  String type;
  String description;
  DateTime createdAt;
  Transaction({
    this.id,
    this.transactionId,
    this.useremail,
    this.year,
    this.payee,
    this.amount,
    this.category,
    this.type,
    this.description,
    this.createdAt,
  });
  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
        id: json["_id"],
        transactionId: json["id"],
        useremail: json["useremail"],
        payee: json["payee"],
        amount: json["amount"],
        year: json["year"],
        category: json["category"],
        type: json["type"],
        description: json["description"],
        createdAt: DateTime.parse(json["createdAt"]),
      );
  Map<String, dynamic> toJson() => {
        "_id": id,
        "id": transactionId,
        "useremail": useremail,
        "payee": payee,
        "amount": amount,
        "year": year,
        "category": category,
        "type": type,
        "description": description,
        "createdAt": createdAt.toIso8601String(),
      };
}
