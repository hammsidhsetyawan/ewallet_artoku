import 'dart:convert';

List<Users> usersFromJson(String str) => List<Users>.from(json.decode(str).map((x) => Users.fromJson(x)));

String usersToJson(List<Users> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Users {
    // List<String> payee;
    String id;
    String firstname;
    String lastname;
    String username;
    String address;
    String useremail;
    String password;
    int userId;
    // DateTime createdAt;
    // DateTime lockedUntil;
    int v;
    String pinUser;
    int failedAttempt;
    int balance;

    Users({
        // this.payee,
        this.id,
        this.firstname,
        this.lastname,
        this.username,
        this.address,
        this.useremail,
        this.password,
        this.userId,
        // this.createdAt,
        // this.lockedUntil,
        this.v,
        this.pinUser,
        this.failedAttempt,
        this.balance,
    });

    factory Users.fromJson(Map<String, dynamic> json) => Users(
        // payee: List<String>.from(json["payee"].map((x) => x)),
        id: json["_id"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        username: json["username"],
        address: json["address"],
        useremail: json["useremail"],
        password: json["password"],
        userId: json["id"],
        // createdAt: DateTime.parse(json["createdAt"]),
        // lockedUntil: DateTime.parse(json["locked_until"]),
        v: json["__v"],
        pinUser: json["pinUser"],
        failedAttempt: json["failed_attempt"],
        balance: json["balance"],
    );

    Map<String, dynamic> toJson() => {
        // "payee": List<dynamic>.from(payee.map((x) => x)),
        "_id": id,
        "firstname": firstname,
        "lastname": lastname,
        "username": username,
        "address": address,
        "useremail": useremail,
        "password": password,
        "id": userId,
        // "createdAt": createdAt.toIso8601String(),
        // "locked_until": lockedUntil.toIso8601String(),
        "__v": v,
        "pinUser": pinUser,
        "failed_attempt": failedAttempt,
        "balance": balance,
    };
}