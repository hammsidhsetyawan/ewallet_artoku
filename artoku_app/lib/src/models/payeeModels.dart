// To parse this JSON data, do
//
//     final payee = payeeFromJson(jsonString);

import 'dart:convert';

List<Payee> payeeFromJson(String str) => List<Payee>.from(json.decode(str).map((x) => Payee.fromJson(x)));

String payeeToJson(List<Payee> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Payee {
    String useremail;
    String username;

    Payee({
        this.useremail,
        this.username,
    });

    factory Payee.fromJson(Map<String, dynamic> json) => Payee(
        useremail: json["useremail"],
        username: json["username"],
    );

    Map<String, dynamic> toJson() => {
        "useremail": useremail,
        "username": username,
    };
}
