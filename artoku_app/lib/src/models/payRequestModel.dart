// To parse this JSON data, do
//
//     final payRequest = payRequestFromJson(jsonString);

import 'dart:convert';

List<PayRequest> payRequestFromJson(String str) =>
    List<PayRequest>.from(json.decode(str).map((x) => PayRequest.fromJson(x)));

String payRequestToJson(List<PayRequest> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PayRequest {
  String id;
  String payRequestId;
  String useremail;
  String emailpayee;
  int amount;
  String description;
  String status;
  DateTime createdAt;
  int v;

  PayRequest({
    this.id,
    this.payRequestId,
    this.useremail,
    this.emailpayee,
    this.amount,
    this.description,
    this.status,
    this.createdAt,
    this.v,
  });

  factory PayRequest.fromJson(Map<String, dynamic> json) => PayRequest(
        id: json["_id"],
        payRequestId: json["id"],
        useremail: json["useremail"],
        emailpayee: json["emailpayee"],
        amount: json["amount"],
        description: json["description"],
        status: json["status"],
        createdAt: DateTime.parse(json["createdAt"]),
        v: json["__v"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "id": payRequestId,
        "useremail": useremail,
        "emailpayee": emailpayee,
        "amount": amount,
        "description": description,
        "status": status,
        "createdAt": createdAt.toIso8601String(),
        "__v": v,
      };
}
